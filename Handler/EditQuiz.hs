{-# LANGUAGE TupleSections, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.EditQuiz 

(
getEditQuizBaseR,
getEditQuizR,
editQuizSessionOpenedCommon,
createQuizJavascript,
getEntityIdVal
)


where

import Import
import Yesod.Auth
import Data.Text (pack, concat)
import Data.List (maximumBy)
import Data.Function (on)
import Database.Persist.Store
import Database.Persist.GenericSql.Raw
import Data.Int
import qualified Data.Map as DM
import Handler.UpdateQuizPendingResults
--import Handler.EditQuizSaveChanges
--import Text.Cassius




getEditQuizBaseR :: Handler RepHtml
getEditQuizBaseR = redirect HomeR


getEditQuizR :: ZQuizBaseId -> Handler RepHtml
getEditQuizR quizBaseId = do
    authEntity <- requireAuth
    let authId = entityKey authEntity
    let authUser = entityVal authEntity
    let userEmailM = userEmail authUser

    maybeQuiz <- runDB $ selectFirst [ZQuizBaseOwningUserId ==. authId, ZQuizBaseId ==. quizBaseId] []

    case maybeQuiz of
        Nothing -> notFound
        Just quizEntity -> editQuizSessionOpenedCommon True quizBaseId quizEntity userEmailM


type Backend = Database.Persist.GenericSql.Raw.SqlPersist


editQuizSessionOpenedCommon :: Bool -> (Key Backend (ZQuizBaseGeneric Backend)) -> (Entity (ZQuizBaseGeneric Backend)) -> Maybe Text -> Handler RepHtml
editQuizSessionOpenedCommon doEdit quizBaseId quizEntity userEmailM = do
            dbLessonEntities <- runDB $ selectList [ZQuizLessonOwningQuizId ==. quizBaseId] []
            if null dbLessonEntities
                then invalidArgs ["Requested quiz has no lessons. Very bad."]
                else
                    defaultLayout $ do
                        let dbQuiz = entityVal quizEntity
                        let quizTitle = zQuizBaseTitle dbQuiz
                       
                        dbEntryEntities <- lift $ runDB $ selectList [ZEntryItemsOwningQuizId ==. quizBaseId] []

                        --let dbLessons = map (\l -> entityVal l) dbLessonEntities
                        --let dbEntries = map (\e -> entityVal e) dbEntryEntities

                        let dbLessons = map getEntityIdVal dbLessonEntities
                        let dbEntries = map getEntityIdVal dbEntryEntities

                        let lessonMap = createLessonMap $ map (\lp -> snd lp) dbLessons
                        let rootLesson = DM.findWithDefault (snd $ DM.elemAt 0 lessonMap) (zQuizBaseRootLesson dbQuiz) lessonMap

                        let flattenedLessonDFSTree = createFlattenedLessonDFSTree rootLesson lessonMap

                        if doEdit
                            then setTitle "Edit Quiz"
                            else setTitle "Session Opened"
                        
                        -- addScript $ StaticR $ StaticRoute ["js", "jquery-1.7.min.js"] []
                        addScript $ StaticR js_jquery_1_7_min_js
                        addScript $ StaticR js_jquery_treeTable_js
                        addScript $ StaticR js_jquery_editable_1_3_3_js
                        addScript $ StaticR js_jquery_hoverIntent_minified_js
                        addScript $ StaticR js_jstorage_js
                        addScript $ StaticR js_jquery_json_2_3_min_js
                        addScript $ StaticR js_date_js

                        if doEdit
                            then toWidget [julius| var isEditableQuiz = true; |]
                            else toWidget [julius| var isEditableQuiz = false; |]

                        createQuizJavascript dbQuiz dbLessons dbEntries quizBaseId

                        --addStylesheet $ StaticR css_jquery_treeTable_css

                        $(widgetFile "Header")

                        $(widgetFile "trySaveCachedData")

                        $(widgetFile "QuizDueCommon")

                        $(widgetFile "EditQuiz")

                        $(widgetFile "Footer")


createQuizJavascript :: ZQuizBaseGeneric backend -> [(Int64, ZQuizLessonGeneric backend)] -> [(Int64, ZEntryItemsGeneric backend)] -> ZQuizBaseId -> Widget
createQuizJavascript dbQuiz dbLessons dbEntries quizBaseId =
        toWidget [julius|   
                            var quizID = #{quizIDVar};
                            var quiz = {
                                        version: #{show $ zQuizBaseVersion dbQuiz},
                                        language: new Array(2),
                                        title: "#{zQuizBaseTitle dbQuiz}",
                                        levels: #{show $ zQuizBaseLeitnerLevels dbQuiz},
                                        rootLesson: #{show $ zQuizBaseRootLesson dbQuiz},
                                        realLeitner: #{boolToText $ zQuizBaseRealLeitner dbQuiz},
                                        minQLocality: #{show $ zQuizBaseMinQuestionLocality dbQuiz},
                                        maxQLocality: #{show $ zQuizBaseMaxQuestionLocality dbQuiz},
                                        showWordCycle: #{boolToText $ zQuizBaseShowWordCycle dbQuiz},
                                        numRepeatUntilAccepted: #{show $ zQuizBaseNumRepeatUntilAccepted dbQuiz},
                                        resetCountOnWrong: #{boolToText $ zQuizBaseResetCountOnWrong dbQuiz},
                                        randomFlashcardMode: #{boolToText $ zQuizBaseRandomFlashcardMode dbQuiz},
                                        maxSessionEntries: #{show $ zQuizBaseMaxSessionEntries dbQuiz},
                                        fontSize: #{show $ zQuizBaseFontSize dbQuiz}
                                     };

                            quiz.language[0] =  "#{zQuizBaseLanguage1 dbQuiz}";
                            quiz.language[1] = "#{zQuizBaseLanguage2 dbQuiz}";

                            var lessons = new Array(#{show $ maxLessonNum baseLessons});
                            var entries = new Array(#{show $ maxEntryNum baseEntries});
                            #{lessonArray dbLessons}
                            #{entryArray dbEntries}
                 |]

        where
            quizIDVar = pack . show $ convertQuizID quizBaseId :: Text
            convertPersistInt (PersistInt64 val) = val
            convertPersistInt _ = -1
            --convertID m_id = convertPersistInt . unKey $ fromJust m_id :: Int64
            convertQuizID qid = convertPersistInt $ unKey qid :: Int64

            lessonArray [] = "" :: Text
            lessonArray lessons = Data.Text.concat $ map lessonToJSObject lessons :: Text
            entryArray [] = "" :: Text
            entryArray entries = Data.Text.concat $ map entryToJSObject entries :: Text

            baseLessons = baseEntities dbLessons
            baseEntries = baseEntities dbEntries
            baseEntities = map (\(_, e) -> e)

            maxLessonNum [] = 1
            maxLessonNum lessons = (+1) . zQuizLessonQuizLessonID $ maximumBy (compare `on` zQuizLessonQuizLessonID) lessons
            maxEntryNum [] = 1
            maxEntryNum entries = (+1) . zEntryItemsQuizEntryID $ maximumBy (compare `on` zEntryItemsQuizEntryID) entries


boolToText :: Bool -> Text
boolToText True = "true"
boolToText False = "false"


getEntityIdVal :: Entity a -> (Int64, a)
getEntityIdVal entity =
        (entityID, entityVal entity)
    where        
        entityID = convertID $ entityKey entity :: Int64
        convertPersistInt (PersistInt64 val) = val
        convertPersistInt _ = -1
        convertID qid = convertPersistInt $ unKey qid :: Int64


lessonToJSObject :: (Int64, ZQuizLessonGeneric backend) -> Text            
lessonToJSObject (uniqueID, lesson) = Data.Text.concat [
    "lessons[", (pack . show $ zQuizLessonQuizLessonID lesson), "] = ",
    "{",
    "  uniqueLessonID: ", (pack $ show uniqueID),
    ", lessonID: ", (pack . show $ zQuizLessonQuizLessonID lesson),
    ", name: \"", (zQuizLessonName lesson),
    "\", category: \"", (zQuizLessonCategory lesson),
    "\", selected: ", (boolToText $ zQuizLessonSelected lesson),
    ", childLessons: ", (pack . show $ zQuizLessonChildLessons lesson),
    ", entries: ", (pack . show $ zQuizLessonEntries lesson),
    ", numDue1: 0, numDue2: 0",
    "};\n"
    ]


entryToJSObject :: (Int64, ZEntryItemsGeneric backend) -> Text            
entryToJSObject (uniqueID, entry) = Data.Text.concat [
        "entries[", (pack . show $ zEntryItemsQuizEntryID entry), "] = ",
        "{",
        "  uniqueEntryID: ", (pack $ show uniqueID),
        ", entryID: ", (pack . show $ zEntryItemsQuizEntryID entry),
        ", count1: ", (pack . show $ zEntryItemsCount1 entry),
        ", count2: ", (pack . show $ zEntryItemsCount2 entry),
        ", grade1: ", (pack . show $ zEntryItemsCurrentGrade1 entry),
        ", grade2: ", (pack . show $ zEntryItemsCurrentGrade2 entry),
        ", errorCount1: ", (pack . show $ zEntryItemsErrorCount1 entry),
        ", errorCount2: ", (pack . show $ zEntryItemsErrorCount2 entry), 
        ", date1: \"", (packDate $ zEntryItemsDate1 entry), "\"",
        ", date2: \"", (packDate $ zEntryItemsDate2 entry), "\"",
        ", text1: \"", (zEntryItemsText1 entry),
        "\", text2: \"", (zEntryItemsText2 entry),
        "\", isDue1: false, isDue2: false",
        "};\n"   
        ]
    where 
        --packDate :: UTCTime -> Text
        packDate timeval =
                                pack $ take ((length timevalStr) - 4) timevalStr
                            where
                                timevalStr = show timeval


createLessonMap :: [ZQuizLessonGeneric backend] -> DM.Map Int64 (ZQuizLessonGeneric backend)
createLessonMap dbLessons =
    DM.fromList $ map (\l -> (zQuizLessonQuizLessonID l, l)) dbLessons


createFlattenedLessonDFSTree :: (ZQuizLessonGeneric backend) -> 
                                DM.Map Int64 (ZQuizLessonGeneric backend) ->
                                [(ZQuizLessonGeneric backend, Int64)]  --lesson to parent lesson id
createFlattenedLessonDFSTree rootLessonVar lessonMap =
    createFlattenedDFSLessonTreeHelper rootLessonVar (-1) lessonMap


createFlattenedDFSLessonTreeHelper ::   (ZQuizLessonGeneric backend) -> 
                                        Int64 ->
                                        DM.Map Int64 (ZQuizLessonGeneric backend) -> 
                                        [(ZQuizLessonGeneric backend, Int64)] --lesson to parent lesson id
createFlattenedDFSLessonTreeHelper lesson parentID lessonMap =
        [(lesson, parentID)] ++ (concatMap createFlattenedChildList (zQuizLessonChildLessons lesson))
    where
        createFlattenedChildList childLessonID =
                case childLessonMaybe of
                    Nothing -> []
                    Just childLesson -> createFlattenedDFSLessonTreeHelper childLesson (zQuizLessonQuizLessonID lesson) lessonMap
            where
                childLessonMaybe = DM.lookup childLessonID lessonMap


editQuizControlWidget :: Bool -> (ZQuizLessonGeneric backend) -> Int64 -> HtmlUrl (Route App)
editQuizControlWidget doEdit lesson parentID =
    [hamlet|
<td>
    <span class="quizTreeRow">

        $if doEdit
            <div class="hideMe quizTreeControlRow">
                \ #
                <span class="controlAction" onclick="handleAddChildLesson(#{show $ zQuizLessonQuizLessonID lesson}, 'l_#{show $ zQuizLessonQuizLessonID lesson}')">
                    <img src=@{StaticR img_addChildLesson_png}>
                    <span class="catooltip">
                        Add Child Lesson
                \ #

                $if (<) parentID 0
                    \ #
                $else
                    \ #
                    <span class="controlAction" onclick="handleRemoveLesson(#{show $ zQuizLessonQuizLessonID lesson}, 'l_#{show $ zQuizLessonQuizLessonID lesson}')">
                        <img src=@{StaticR img_deleteLesson_png}>
                        <span class="catooltip">
                            Delete Lesson
                    \ #

                \ #
                <span class="controlAction" onclick="handleRenameLesson(#{show $ zQuizLessonQuizLessonID lesson}, 'l_#{show $ zQuizLessonQuizLessonID lesson}')">
                    <img src=@{StaticR img_renameLesson_png}>
                    <span class="catooltip">
                        Rename Lesson
                \ #

                \ #
                <span class="controlAction" onclick="handleEditLessonCategory(#{show $ zQuizLessonQuizLessonID lesson}, 'l_#{show $ zQuizLessonQuizLessonID lesson}')">
                    <img src=@{StaticR img_editCategory_png}>
                    <span class="catooltip">
                        Edit Lesson Category
                \ #
        $else
            \ #

        <span class="quizTreeRowText" uiID="#{show $ zQuizLessonQuizLessonID lesson}" rowId="l_#{show $ zQuizLessonQuizLessonID lesson}">

            $if (==) "" (zQuizLessonCategory lesson)
                #{zQuizLessonName lesson}
            $else
                #{zQuizLessonName lesson} { #{zQuizLessonCategory lesson} }

<td style="text-align: center; vertical-align: middle;">
    <input class="selection_checkbox" type="checkbox" name="selected"
        onclick="handleSelectedCheckChange(this, #{show $ zQuizLessonQuizLessonID lesson}, 'l_#{show $ zQuizLessonQuizLessonID lesson}')" :zQuizLessonSelected lesson:checked :not doEdit:disabled  >

$if not doEdit
    <td class="numDue1">
        0
    <td class="numDue2">
        0
$else
    #

    |]







