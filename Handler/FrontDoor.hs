{-# LANGUAGE TupleSections, OverloadedStrings, BangPatterns #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}


    
module Handler.FrontDoor 
(
getFrontDoorR
)

where

import Import
import Yesod.Auth
import Database.Persist.Store
import Data.Int
import Handler.UpdateQuizPendingResults
import KVTML
import Data.List (head)
import Data.Either (rights)
import Data.ByteString.Lazy (readFile)
import Debug.Trace
import Data.Maybe
import System.IO.Unsafe
import Settings


getFrontDoorR :: Handler RepHtml
getFrontDoorR = do
        authEntity <- requireAuth
        let authId = entityKey authEntity
        let authUser = entityVal authEntity
        let userEmailM = userEmail authUser

        firstLogin <- isFirstLogin authId authUser

        -- ... add due counts (offload work to javascript)

        defaultLayout $ do
            if firstLogin
                then do
                    katakanaBS <- liftIO $ readFile $ staticDir ++ "/katakana.kvtml"
                    let importedKVTMLE = importKVTML katakanaBS
                    lift $ saveNewKVTML (head $ rights [importedKVTMLE]) authId
                else return $ promoteToID 0

            quizes <- lift $ runDB $ selectList [ZQuizBaseOwningUserId ==. authId] []
            let quizIDTitles = map getQuizIDTitle quizes

            setTitle "Welcome to Word Quizler"

            addScript $ StaticR js_jquery_1_7_min_js
            addScript $ StaticR js_jstorage_js
            addScript $ StaticR js_jquery_json_2_3_min_js

            $(widgetFile "Header")

            $(widgetFile "trySaveCachedData")

            $(widgetFile "Frontdoor")

            $(widgetFile "Footer")
    where
        promoteToID :: Int64 -> Key (YesodPersistBackend App) a
        promoteToID int64Val = Key $ PersistInt64 int64Val


getQuizIDTitle :: Entity (ZQuizBaseGeneric backend) -> (Key backend (ZQuizBaseGeneric backend), Text)
getQuizIDTitle quiz = 
        (quizID, quizTitle)
    where
        quizTitle = zQuizBaseTitle $ entityVal quiz
        quizID = entityKey quiz


isFirstLogin :: Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> UserGeneric (YesodPersistBackend App) -> Handler Bool
isFirstLogin authId authUser = do
    if (userJustCreated authUser) == False
        then return False
        else do
            runDB $ updateWhere [UserId ==. authId] [UserJustCreated =. False]
            return True




