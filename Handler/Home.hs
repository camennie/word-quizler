{-# LANGUAGE TupleSections, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.Home where

import Import
import Yesod.Auth


getHomeR :: Handler RepHtml
getHomeR = do
        maid <- maybeAuthId
        case maid of
            Nothing -> 
                defaultLayout $ do
                    addStylesheet $ StaticR css_highslide_css

                    addScript $ StaticR js_jquery_1_7_min_js
                    addScript $ StaticR js_highslide_with_gallery_js

                    setTitle "Welcome to Word Quizler"

                    $(widgetFile "Header_NoLogin")

                    $(widgetFile "Home")

                    $(widgetFile "Footer")

            _ -> redirect FrontDoorR








