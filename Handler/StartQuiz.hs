{-# LANGUAGE TupleSections, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.StartQuiz

where

import Import
import Yesod.Auth
import Database.Persist.Store
import Data.Int
import Handler.EditQuiz
import Handler.SessionOpened


getStartQuizR :: ZQuizBaseId -> Int -> Handler RepHtml
getStartQuizR quizBaseId startLanguage = do
    authEntity <- requireAuth
    let authId = entityKey authEntity
    let authUser = entityVal authEntity
    let userEmailM = userEmail authUser

    maybeQuiz <- runDB $ selectFirst [ZQuizBaseOwningUserId ==. authId, ZQuizBaseId ==. quizBaseId] []

    case maybeQuiz of
        Nothing -> notFound
        Just quizEntity -> do
            dbLessonEntities <- runDB $ selectList [ZQuizLessonOwningQuizId ==. quizBaseId] []
            if null dbLessonEntities
                then invalidArgs ["Requested quiz has no lessons. Very bad."]
                else
                    defaultLayout $ do
                        let dbQuiz = entityVal quizEntity
                        let quizTitle = zQuizBaseTitle dbQuiz
                       
                        dbEntryEntities <- lift $ runDB $ selectList [ZEntryItemsOwningQuizId ==. quizBaseId] []

                        let dbLessons = map getEntityIdVal dbLessonEntities
                        let dbEntries = map getEntityIdVal dbEntryEntities

                        addScript $ StaticR js_jquery_1_7_min_js
                        addScript $ StaticR js_date_js
                        addScript $ StaticR js_jstorage_js
                        addScript $ StaticR js_jquery_json_2_3_min_js

                        toWidget [julius| var startLanguage = #{show startLanguage}; |]

                        createQuizJavascript dbQuiz dbLessons dbEntries quizBaseId

                        setTitle "Welcome to Word Quizler"
                        $(widgetFile "Header")

                        $(widgetFile "QuizDueCommon")

                        $(widgetFile "StartQuiz")

                        $(widgetFile "Footer")


