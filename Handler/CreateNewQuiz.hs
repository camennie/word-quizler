{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.CreateNewQuiz where

import GHC.Generics
import Import
import KVTML
--import Yesod.Json
import Yesod.Auth
import Data.Aeson hiding (object)
import Database.Persist.Store
import Data.Int


data NewQuizOkResponse = NewQuizOkResponse { succeeded :: Bool, message :: Text, quizID :: Int64 } deriving Generic


instance ToJSON NewQuizOkResponse



createNewQuizOkResponse :: Int64 -> NewQuizOkResponse
createNewQuizOkResponse m_id = 
    NewQuizOkResponse { succeeded = True, message = "All Good", quizID = m_id }


createNewQuizNoSessionResponse :: NewQuizOkResponse
createNewQuizNoSessionResponse = 
    NewQuizOkResponse { succeeded = False, message = "Session Expired", quizID = -1 }


postCreateNewQuizR :: Handler RepJson
postCreateNewQuizR = do
        maid <- maybeAuthId
        case maid of
            Just actualId -> do
                newQuizID <- saveNewKVTML defaultKVTML actualId
                jsonToRepJson . createNewQuizOkResponse $ convertQuizID newQuizID
            _ ->
                jsonToRepJson createNewQuizNoSessionResponse
    where
        convertPersistInt (PersistInt64 val) = val
        convertPersistInt _ = -1
        --convertID m_id = convertPersistInt . unKey $ fromJust m_id :: Int64
        convertQuizID qid = convertPersistInt $ unKey qid :: Int64










