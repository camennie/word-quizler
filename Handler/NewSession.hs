{-# LANGUAGE TupleSections, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.NewSession where

import Import
import Yesod.Auth


getNewSessionR :: Handler RepHtml
getNewSessionR = do
    authEntity <- requireAuth
    let authId = entityKey authEntity
    let authUser = entityVal authEntity
    let userEmailM = userEmail authUser

    ((_, uploadKvtmlFormWidget), uploadKvtmlFormEnctype) <- runFormPost uploadKVTMLForm

    defaultLayout $ do
        setTitle "Create New Session"
        
        -- addScript $ StaticR $ StaticRoute ["js", "jquery-1.7.min.js"] []
        addScript $ StaticR js_jquery_1_7_min_js

        $(widgetFile "Header")

        $(widgetFile "NewSession")

        $(widgetFile "Footer")



data UploadKVTML = UploadKVTML { fileinfo :: FileInfo }

--uploadKVTMLForm :: Html -> MForm App App (FormResult (b -> (FileInfo, b)), Widget)
uploadKVTMLForm = renderDivs $ UploadKVTML -- pure (,)
                    -- <*> areq textField "TextField" Nothing
                    -- <*> fileAFormReq "Filename: "
                    <$> fileAFormReq "Filename: "







