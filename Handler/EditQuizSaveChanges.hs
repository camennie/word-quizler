{-# LANGUAGE TupleSections, OverloadedStrings, DeriveGeneric #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.EditQuizSaveChanges where

import Import
import Yesod.Auth
import Data.Time
import Data.Text (pack, concat, unpack)
import Data.Text.Encoding (encodeUtf8)
import Data.List (maximumBy)
import Data.Function (on)
import Data.Aeson hiding (object)
import Data.Attoparsec
import GHC.Generics
import Database.Persist.Store
import Data.Int
import qualified Data.Map as DM
import Control.Applicative
import Control.Monad
import Debug.Trace
import Data.ByteString (ByteString)
import Database.Persist.Store
import Data.Conduit
import Data.Conduit.List (mapM_)


data EditQuizSaveChangesResponse = EditQuizSaveChangesResponse { succeeded :: Bool, message :: Text } deriving Generic
instance ToJSON EditQuizSaveChangesResponse


data QuizSaveChangesLesson = QuizSaveChangesLesson {
                                                        uniqueLessonID :: Int64,
                                                        lessonID :: Int64,
                                                        name :: Text,
                                                        category :: Text,
                                                        selected :: Bool,
                                                        childLessons :: [Int64],
                                                        entries :: [Int64]
                                                   } deriving (Eq, Show)


instance FromJSON QuizSaveChangesLesson
    where
        parseJSON (Object v) = QuizSaveChangesLesson <$>
                                            v .: "uniqueLessonID" <*>
                                            v .: "lessonID" <*>
                                            v .: "name" <*>
                                            v .: "category" <*>
                                            v .: "selected" <*>
                                            v .: "childLessons" <*>
                                            v .: "entries"

        parseJSON _ = mzero


data QuizSaveChangesEntry = QuizSaveChangesEntry {
                                                    uniqueEntryID :: Int64,
                                                    entryID :: Int64,
                                                    count1 :: Int,
                                                    count2 :: Int,
                                                    grade1 :: Int,
                                                    grade2 :: Int,
                                                    errorCount1 :: Int,
                                                    errorCount2 :: Int,
                                                    date1 :: Text,
                                                    date2 :: Text,
                                                    text1 :: Text,
                                                    text2 :: Text
                                                 } deriving (Eq, Show)

instance FromJSON QuizSaveChangesEntry
    where
        parseJSON (Object v) = QuizSaveChangesEntry <$>
                                            v .: "uniqueEntryID" <*>
                                            v .: "entryID" <*>
                                            v .: "count1" <*>
                                            v .: "count2" <*>
                                            v .: "grade1" <*>
                                            v .: "grade2" <*>
                                            v .: "errorCount1" <*>
                                            v .: "errorCount2" <*>
                                            v .: "date1" <*>
                                            v .: "date2" <*>
                                            v .: "text1" <*>
                                            v .: "text2"

        parseJSON _ = mzero


data QuizSaveChanges = QuizSaveChanges {
                                            quizID :: Int64,
                                            version :: Int,
                                            title :: Text,
                                            language1 :: Text,
                                            language2 :: Text,
                                            levels :: [Int],
                                            rootLesson :: Int64,
                                            realLeitner :: Bool,
                                            minQLocality :: Int,
                                            maxQLocality :: Int,
                                            showWordCycle :: Bool,
                                            numRepeatUntilAccepted :: Int,
                                            resetCountOnWrong :: Bool,
                                            randomFlashcardMode :: Bool,
                                            maxSessionEntries :: Int,
                                            fontSize :: Double,
                                            modifiedEntries :: [QuizSaveChangesEntry],
                                            newEntries :: [QuizSaveChangesEntry],
                                            modifiedLessons :: [QuizSaveChangesLesson],
                                            newLessons :: [QuizSaveChangesLesson]
                                        } deriving (Eq, Show)

instance FromJSON QuizSaveChanges
    where
        parseJSON (Object v) = QuizSaveChanges <$>
                                            v .: "quizID" <*>
                                            v .: "version" <*>
                                            v .: "title" <*>
                                            v .: "language1" <*>
                                            v .: "language2" <*>
                                            v .: "levels" <*>
                                            v .: "rootLesson" <*>
                                            v .: "realLeitner" <*>
                                            v .: "minQLocality" <*>
                                            v .: "maxQLocality" <*>
                                            v .: "showWordCycle" <*>
                                            v .: "numRepeatUntilAccepted" <*>
                                            v .: "resetCountOnWrong" <*>
                                            v .: "randomFlashcardMode" <*>
                                            v .: "maxSessionEntries" <*>
                                            v .: "fontSize" <*>
                                            v .: "modifiedEntries" <*>
                                            v .: "newEntries" <*>
                                            v .: "modifiedLessons" <*>
                                            v .: "newLessons"

        parseJSON _ = mzero


postEditQuizSaveChangesR :: Handler RepJson
postEditQuizSaveChangesR = do
    maid <- maybeAuthId
    case maid of
        Just actualId -> do
            postedQuizData <- runInputPost $ ireq textField "quiz"

            let quizSaveChangesE = parseQuizSaveChanges (encodeUtf8 postedQuizData) 
            --jsonToRepJson $ EditQuizSaveChangesResponse { succeeded = False, message = "Session Expired. Reload in another tab." }
            
            case quizSaveChangesE of
                Left msg -> jsonToRepJson $ EditQuizSaveChangesResponse { succeeded = False, message = msg }
                Right quizSaveChanges -> do
                    saveResult <- saveQuizChanges actualId quizSaveChanges
                    case saveResult of
                        Left msg -> jsonToRepJson $ EditQuizSaveChangesResponse { succeeded = False, message = msg }
                        Right _ -> jsonToRepJson $ EditQuizSaveChangesResponse { succeeded = True, message = "All good" }

        _ -> jsonToRepJson $ EditQuizSaveChangesResponse { succeeded = False, message = "Session Expired. Reload in another tab." }

    where
        parseQuizSaveChanges :: ByteString -> Either Text QuizSaveChanges
        parseQuizSaveChanges content =                                 
                                            case parsedJSON of   -- <$> is fmap
                                                Data.Attoparsec.Done _ (Success savedData) -> Right savedData
                                                Data.Attoparsec.Done _ (Error err) -> Left $ pack err
                                        where
                                            parsedJSON = (parse (fromJSON <$> json) content) :: Data.Attoparsec.Result (Data.Aeson.Result QuizSaveChanges)


saveQuizChanges :: Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> QuizSaveChanges -> Handler (Either Text Text)
saveQuizChanges userID quizSaveChanges = do
        -- verify quizid is a quiz for user    
        quizList <- runDB $ selectList [ZQuizBaseId ==. curQuizID, ZQuizBaseOwningUserId ==. userID] [] 
        case quizList of
            [curQuiz] -> do 
                            runDB $ (
                                     (addNewItems $ map (convertLesson curQuizID) (newLessons quizSaveChanges)) >> 
                                     (addNewItems $ map (convertEntry curQuizID) (newEntries quizSaveChanges)) >>
                                     (updateLessonItems $ modifiedLessons quizSaveChanges) >>
                                     (updateEntryItems $ modifiedEntries quizSaveChanges) >>
                                     (replace curQuizID $ convertQuiz userID quizSaveChanges)
                                    )
                            
                            return $ Right "All good"
                        where
                            --realModifiedLessons :: [ZQuizLesson]
                            --realModifiedLessons = map (convertLesson curQuizID) (modifiedLessons quizSaveChanges)
                            
                            updateLessonItems [] = return ()
                            updateLessonItems (x:xs) = (replace (promoteToID $ uniqueLessonID x) $ convertLesson curQuizID x) >> updateLessonItems xs

                            updateEntryItems [] = return ()
                            updateEntryItems (x:xs) = (replace (promoteToID $ uniqueEntryID x) $ convertEntry curQuizID x) >> updateEntryItems xs

                            addNewItems [] = return ()
                            addNewItems (x:xs) = insert x >> addNewItems xs

            _ -> return $ Left "No matching quiz found for user."
        
    where
        --curQuizID :: Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App))
        --curQuizID = Key . PersistInt64 $ quizID quizSaveChanges

        curQuizID = promoteToID $ quizID quizSaveChanges

        promoteToID :: Int64 -> Key (YesodPersistBackend App) a
        promoteToID int64Val = Key $ PersistInt64 int64Val 


{-
--updateLessonItems :: Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App)) -> [ZQuizLesson] -> ResourceT (ResourceT m) ()
updateLessonItems curQuizID [] = return ()
updateLessonItems curQuizID (actualLesson:xs) =                                     
        ((selectKeys   [ZQuizLessonOwningQuizId ==. curQuizID, 
                        ZQuizLessonQuizLessonID ==. (zQuizLessonQuizLessonID actualLesson)]) $$ updateLessonSink) >>
        (updateLessonItems curQuizID xs)
    where
        updateLessonSink = Data.Conduit.List.mapM_ (\k -> replace k actualLesson)
        --updateLessonSink lessonKey = return $ StateDone Nothing (replace lessonKey actualLesson)
-}


convertLesson :: Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App)) -> QuizSaveChangesLesson -> ZQuizLesson
convertLesson curQuizID qscLesson  =
                ZQuizLesson {
                    zQuizLessonOwningQuizId = curQuizID,
                    zQuizLessonQuizLessonID = lessonID qscLesson,
                    zQuizLessonName = name qscLesson,
                    zQuizLessonCategory = category qscLesson,
                    zQuizLessonSelected = selected qscLesson,
                    zQuizLessonChildLessons = childLessons qscLesson,
                    zQuizLessonEntries = entries qscLesson
                }


convertEntry :: Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App)) -> QuizSaveChangesEntry -> ZEntryItems
convertEntry curQuizID qscEntry  =
                ZEntryItems {
                    zEntryItemsOwningQuizId = curQuizID,
                    zEntryItemsQuizEntryID = entryID qscEntry,
                    zEntryItemsCount1 = count1 qscEntry,
                    zEntryItemsCount2 = count2 qscEntry,
                    zEntryItemsCurrentGrade1 = grade1 qscEntry,
                    zEntryItemsCurrentGrade2 = grade2 qscEntry,
                    zEntryItemsErrorCount1 = errorCount1 qscEntry,
                    zEntryItemsErrorCount2 = errorCount2 qscEntry,
                    zEntryItemsDate1 = actualDate1,
                    zEntryItemsDate2 = actualDate2,
                    zEntryItemsText1 = text1 qscEntry,
                    zEntryItemsText2 = text2 qscEntry 
                }
            where
                actualDate1 = read . unpack $ date1 qscEntry
                actualDate2 = read . unpack $ date2 qscEntry


convertQuiz :: Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> QuizSaveChanges -> ZQuizBase
convertQuiz userID qsChanges =
        ZQuizBase {
            zQuizBaseVersion = 1,
            zQuizBaseOwningUserId = userID,
            zQuizBaseLanguage1 = language1 qsChanges,
            zQuizBaseLanguage2 = language2 qsChanges,
            zQuizBaseTitle = title qsChanges,
            zQuizBaseLeitnerLevels = levels qsChanges,
            zQuizBaseRootLesson = rootLesson qsChanges,
            zQuizBaseRealLeitner = realLeitner qsChanges,
            zQuizBaseMinQuestionLocality = minQLocality qsChanges,
            zQuizBaseMaxQuestionLocality = maxQLocality qsChanges,
            zQuizBaseShowWordCycle = showWordCycle qsChanges,
            zQuizBaseNumRepeatUntilAccepted = numRepeatUntilAccepted qsChanges,
            zQuizBaseResetCountOnWrong = resetCountOnWrong qsChanges,
            zQuizBaseRandomFlashcardMode = randomFlashcardMode qsChanges,
            zQuizBaseMaxSessionEntries = maxSessionEntries qsChanges,
            zQuizBaseFontSize = fontSize qsChanges
        }


                                           
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            



