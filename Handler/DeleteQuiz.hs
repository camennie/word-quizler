{-# LANGUAGE TupleSections, OverloadedStrings, DeriveGeneric #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.DeleteQuiz where

import Import
import Yesod.Auth
import Data.Time
import Data.Text (pack, concat, unpack)
import Data.Text.Encoding (encodeUtf8)
import Data.List (maximumBy)
import Data.Function (on)
import Data.Aeson hiding (object)
import Data.Attoparsec
import GHC.Generics
import Database.Persist.Store
import Data.Int
import qualified Data.Map as DM
import Control.Applicative
import Control.Monad
import Debug.Trace
import Data.ByteString (ByteString)
import Database.Persist.Store
import Data.Conduit
import Data.Conduit.List (mapM_)


data DeleteQuizResponse = DeleteQuizResponse { succeeded :: Bool, message :: Text } deriving Generic
instance ToJSON DeleteQuizResponse


postDeleteQuizBaseR :: Handler RepHtml
postDeleteQuizBaseR = redirect HomeR


postDeleteQuizR :: ZQuizBaseId -> Handler RepJson
postDeleteQuizR quizBaseId = do
        maid <- maybeAuthId
        case maid of
            Just actualId -> do
                deleteResult <- deleteQuiz actualId quizBaseId
                case deleteResult of
                    Left msg -> jsonToRepJson $ DeleteQuizResponse { succeeded = False, message = msg }
                    Right _ -> jsonToRepJson $ DeleteQuizResponse { succeeded = True, message = "All good" }

            _ ->
                jsonToRepJson $ DeleteQuizResponse { succeeded = False, message = "Session Expired" }


deleteQuiz :: Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> ZQuizBaseId -> Handler (Either Text Text)
deleteQuiz userID quizBaseId = do
        quizList <- runDB $ selectList [ZQuizBaseId ==. quizBaseId, ZQuizBaseOwningUserId ==. userID] [] 
        case quizList of
            [curQuiz] -> do 
                            runDB $ (
                                     (deleteWhere [ZEntryItemsOwningQuizId ==. quizBaseId]) >>
                                     (deleteWhere [ZQuizLessonOwningQuizId ==. quizBaseId]) >>
                                     (deleteWhere [ZQuizBaseId ==. quizBaseId, ZQuizBaseOwningUserId ==. userID])
                                    )
                            
                            return $ Right "All good"

            _ -> return $ Left "No matching quiz found for user."






