{-# LANGUAGE TupleSections, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.SessionOpened where

import Import
import Yesod.Auth
import Data.Text (pack, concat)
import Data.List (maximumBy)
import Data.Function (on)
import Database.Persist.Store
import Data.Int
import qualified Data.Map as DM
import Handler.EditQuiz
--import Text.Cassius



--getEditQuizBaseR :: Handler RepHtml
--getEditQuizBaseR = redirect HomeR


getSessionOpenedR :: ZQuizBaseId -> Handler RepHtml
getSessionOpenedR quizBaseId = do
    authEntity <- requireAuth
    let authId = entityKey authEntity
    let authUser = entityVal authEntity
    let userEmailM = userEmail authUser

    maybeQuiz <- runDB $ selectFirst [ZQuizBaseOwningUserId ==. authId, ZQuizBaseId ==. quizBaseId] []

    case maybeQuiz of
        Nothing -> notFound
        Just quizEntity -> editQuizSessionOpenedCommon False quizBaseId quizEntity userEmailM




