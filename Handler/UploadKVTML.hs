{-# LANGUAGE TupleSections, OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.UploadKVTML where

import Import
import Yesod.Auth
import Handler.NewSession (uploadKVTMLForm, fileinfo)
import KVTML
--import qualified Data.ByteString.Lazy as L
--import Data.Enumerator.List (consume)


postUploadKVTMLR :: Handler RepHtml
postUploadKVTMLR = do
    authEntity <- requireAuth
    let authId = entityKey authEntity
    let authUser = entityVal authEntity
    let userEmailM = userEmail authUser

    ((result, widget), enctype) <- runFormPost uploadKVTMLForm

    case result of
        FormSuccess uploadKVTML ->
            defaultLayout $ do
                let kvtml_fileName = fileName $ fileinfo uploadKVTML
                
                setTitle "Upload KVTML File"
                
                -- addScript $ StaticR $ StaticRoute ["js", "jquery-1.7.min.js"] []
                addScript $ StaticR js_jquery_1_7_min_js

                $(widgetFile "Header")

                let importedKVTML = importKVTML . fileContent $ fileinfo uploadKVTML
                case importedKVTML of
                    Left errmsg -> 
                        $(widgetFile "UploadKVTML_Error")

                    Right kvtml -> do
                        quizID <- lift $ saveNewKVTML kvtml authId
                        $(widgetFile "UploadKVTML")

                $(widgetFile "Footer")

        _ -> invalidArgs ["Uploaded file was not (or invalid) KVTML"]








