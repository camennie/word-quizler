{-# LANGUAGE TupleSections, OverloadedStrings, DeriveGeneric #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Handler.UpdateQuizPendingResults where

import GHC.Generics
import Import
import KVTML
--import Yesod.Json
import Yesod.Auth
import Data.Aeson hiding (object)
import Database.Persist.Store
import Data.Int
import Data.Time
import Data.Text (pack, concat, unpack)
import Data.Text.Encoding (encodeUtf8)
import Data.List (maximumBy)
import Data.Function (on)
import Data.Attoparsec
import GHC.Generics
import qualified Data.Map as DM
import Control.Applicative
import Control.Monad
import Debug.Trace
import Data.ByteString (ByteString)
import Database.Persist.Store
import Data.Conduit
import Data.Conduit.List (mapM_)



data PostUpdateQuizPendingResults = PostUpdateQuizPendingResults { succeeded :: Bool, message :: Text } deriving Generic
instance ToJSON PostUpdateQuizPendingResults


data PendingResultEntry = PendingResultEntry {
                                                uniqueEntryID :: Int64,
                                                grade1 :: Int,
                                                grade2 :: Int,
                                                date1 :: String,
                                                date2 :: String
                                             } deriving (Eq, Show)

instance FromJSON PendingResultEntry
    where
        parseJSON (Object v) = PendingResultEntry <$>
                                            v .: "uniqueEntryID" <*>
                                            v .: "grade1" <*>
                                            v .: "grade2" <*>
                                            v .: "date1" <*>
                                            v .: "date2"

        parseJSON _ = mzero


postUpdateQuizPendingResultsBaseR :: Handler RepJson
postUpdateQuizPendingResultsBaseR = redirect HomeR


postUpdateQuizPendingResultsR :: ZQuizBaseId -> Handler RepJson
postUpdateQuizPendingResultsR quizBaseId = do
        maid <- maybeAuthId
        case maid of
            Just actualId -> do
                postedEntryUpdateData <- runInputPost $ ireq textField "entryData"
                let entryUpdateDataE = parseEntryUpdateData (encodeUtf8 postedEntryUpdateData)

                case entryUpdateDataE of
                    Left msg -> jsonToRepJson $ PostUpdateQuizPendingResults { succeeded = False, message = msg }
                    Right updatedEntities -> do
                        updateResult <- saveUpdatedEntities actualId quizBaseId updatedEntities
                        case updateResult of
                            Left msg -> jsonToRepJson $ PostUpdateQuizPendingResults { succeeded = False, message = msg }
                            Right _ -> jsonToRepJson $ PostUpdateQuizPendingResults { succeeded = True, message = "All good" }

            _ ->
                jsonToRepJson $ PostUpdateQuizPendingResults { succeeded = False, message = "Session Expired" }
    where
        parseEntryUpdateData :: ByteString -> Either Text [PendingResultEntry]
        parseEntryUpdateData content =                                 
                                            case parsedJSON of   -- <$> is fmap
                                                Data.Attoparsec.Done _ (Success entryData) -> Right entryData
                                                Data.Attoparsec.Done _ (Error err) -> Left $ pack err
                                        where
                                            parsedJSON = (parse (fromJSON <$> json) content) :: Data.Attoparsec.Result (Data.Aeson.Result [PendingResultEntry])


saveUpdatedEntities :: Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> ZQuizBaseId -> [PendingResultEntry] -> Handler (Either Text Text)
saveUpdatedEntities userID quizBaseId updatedEntities = do
        quizList <- runDB $ selectList [ZQuizBaseId ==. quizBaseId, ZQuizBaseOwningUserId ==. userID] [] 
        case quizList of
            [curQuiz] -> do 
                            runDB $ (
                                     (updateEntryItems updatedEntities)
                                    )
                            
                            return $ Right "All good"
                        where
                            updateEntryItems [] = return ()
                            updateEntryItems (x:xs) = (updateWhere [ZEntryItemsId ==. (promoteToID $ uniqueEntryID x), ZEntryItemsOwningQuizId ==. quizBaseId]
                                                        [ZEntryItemsCurrentGrade1 =. (grade1 x),
                                                         ZEntryItemsCurrentGrade2 =. (grade2 x),
                                                         ZEntryItemsDate1 =. (read $ date1 x),
                                                         ZEntryItemsDate2 =. (read $ date2 x)
                                                        ]
                                                      ) >> updateEntryItems xs

            _ -> return $ Left "No matching quiz found for user."
    where
        promoteToID :: Int64 -> Key (YesodPersistBackend App) a
        promoteToID int64Val = Key $ PersistInt64 int64Val
















