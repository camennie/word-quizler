
###
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.
###



$(document).ready ->
    hs.graphicsDir = '@{HomeR}/static/img/graphics/'
    hs.align = 'center'
    hs.transitions = ['expand', 'crossfade']
    hs.wrapperClassName = 'dark borderless floating-caption'
    hs.fadeInOut = true
    hs.dimmingOpacity = 0.75
    hs.creditsText = ""
    hs.creditsTitle = ""
     
    # Add the controlbar
    if hs.addSlideshow?
        hs.addSlideshow({
            #slideshowGroup: 'group1',
            interval: 5000,
            repeat: false,
            useControls: true,
            fixedControls: 'fit',
            overlayOptions: {
                opacity: .6,
                position: 'bottom center',
                hideOnMouseOut: true
            }
        })




