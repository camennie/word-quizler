
###
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.
###



$(document).ready ->
    $("#newSessionA").click(createNewSession)


createNewSession = ->
    $.ajax({
        url: "@{CreateNewQuizR}",
        type: "POST",
        data : {},
        dataType : "json",
        async : true,
        cache : false,

        success : (result, textStatus, jqXHR) ->
            if result.succeeded == true
                window.location.href="@{EditQuizBaseR}/" + result.quizID
            else
                alert("Fail on quiz creation: " + result.message)

        error : (jqXHR, textStatus, errorThrown) ->
            alert("Error on quiz creation: " + textStatus)
    })

