
###
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.
###



editQuizVersion = 5

setupTreeHoverMouseDown = ->
    hiState = {
        interval: 700,
        over:   ->
                    $(this).addClass("quizTreeHover")
                    $(this).children(".quizTreeControlRow").removeClass("hideMe")
                ,
        out:    ->
                    #$(this).removeClass("quizTreeHover")
                    #$(this).children(".quizTreeControlRow").addClass("hideMe")
    }

    $(".quizTreeRow").hoverIntent(hiState)

    $(".quizTreeRow").hover(
        ->
            #$(this).addClass("quizTreeHover")
            #$(this).children(".quizTreeControlRow").removeClass("hideMe")
        ,
        ->
            $(this).removeClass("quizTreeHover")
            $(this).children(".quizTreeControlRow").addClass("hideMe")
    )

    $(".quizTreeRowText").mousedown -> quizTreeRowTextMouseDown($(this))


window.modifiedEntries = new Object()
window.newEntries = new Object()

window.modifiedLessons = new Object()
window.newLessons = new Object()


$(document).ready ->
    $("#quizTree").treeTable()
    setupTreeHoverMouseDown()
    firstRow = $("#l_0 .quizTreeRowText")
    quizTreeRowTextMouseDown(firstRow)
    $("#l_0").toggleBranch()
    $("#quizTreeDiv").removeClass("hiddenClass")

    window.determineDueEntries()
    updateDueValues()
    window.trySaveCachedData()


window.handleDoRenameLesson = ->
    $("#renameLessonPopUp").hide()
    newLessonName = $("#renameLessonPopUpText").val()
    if (newLessonName.length > 0) and (lessons[window.renameLesson_uiID].name != newLessonName)
        lessons[window.renameLesson_uiID].name = newLessonName
                
        lessonText = lessons[window.renameLesson_uiID].name

        if lessons[window.renameLesson_uiID].hasOwnProperty('category')
            if lessons[window.renameLesson_uiID].category.length > 0
                lessonText = lessonText + " { " + lessons[window.renameLesson_uiID].category + " }"
        $("#" + window.renameLesson_rowId + " .quizTreeRowText").text(lessonText)

        window.modifiedLessons[window.renameLesson_uiID] = lessons[window.renameLesson_uiID]


window.handleDoEditLessonCategory = ->
    $("#editLessonCategoryPopUp").hide()
    newLessonCategory = $("#editLessonCategoryPopUpText").val()

    if lessons[window.editLessonCategory_uiID].hasOwnProperty('category')
        lessons[window.editLessonCategory_uiID].category = newLessonCategory
    else
        lessons[window.editLessonCategory_uiID]["category"] = newLessonCategory

    lessonText = lessons[window.editLessonCategory_uiID].name
    if lessons[window.editLessonCategory_uiID].hasOwnProperty('category')
        if lessons[window.editLessonCategory_uiID].category.length > 0
            lessonText = lessonText + " { " + lessons[window.editLessonCategory_uiID].category + " }"
    $("#" + window.editLessonCategory_rowId + " .quizTreeRowText").text(lessonText)

    window.modifiedLessons[window.editLessonCategory_uiID] = lessons[window.editLessonCategory_uiID]


window.handleRenameLesson = (uiID, rowId) ->
    window.renameLesson_uiID = uiID
    window.renameLesson_rowId = rowId

    curRow = $("#" + rowId)
    curRowTextSpanText = lessons[window.renameLesson_uiID].name

    $("#renameLessonPopUpText").val(curRowTextSpanText)

    rowPosition = curRow.position()
    rowHeight = curRow.height()
    rowWidth = curRow.width()
    $("#renameLessonPopUp").css({"left": (rowPosition.left + rowWidth * 0.2) + "px", "top": (rowPosition.top + rowHeight * 1.5) + "px"}).show()


window.handleEditLessonCategory = (uiID, rowId) ->
    window.editLessonCategory_uiID = uiID
    window.editLessonCategory_rowId = rowId

    curRow = $("#" + rowId)
    curRowTextSpanText = ""
    
    if lessons[window.editLessonCategory_uiID].hasOwnProperty('category')
        curRowTextSpanText = lessons[window.editLessonCategory_uiID].category

    $("#editLessonCategoryPopUpText").val(curRowTextSpanText)

    rowPosition = curRow.position()
    rowHeight = curRow.height()
    rowWidth = curRow.width()
    $("#editLessonCategoryPopUp").css({"left": (rowPosition.left + rowWidth * 0.2) + "px", "top": (rowPosition.top + rowHeight * 1.5) + "px"}).show()


window.handleRemoveLesson = (uiID, rowId) ->
    childrenOf = (node) ->
        $(node).siblings("tr." + $.fn.treeTable.defaults.childPrefix + node.attr("id"))

    removeTableRow = (node) ->
        childrenOf(node).each( (index, value) -> removeTableRow($(this)) )
        node.remove()

    removeLessonAndChildren = (uiID) ->
        for childLesson in lessons[uiID].childLessons
            removeLessonAndChildren(childLesson)

        lessons[uiID] = undefined #FIXME: We don't remove this from DB

    removeLessonChildFromParent = (uiID, trRow) ->
        parentID = -1
        trRowClasses = trRow.attr("class").split(" ")
        for classIter in trRowClasses
            if classIter.indexOf($.fn.treeTable.defaults.childPrefix + "l_") == 0
                parentIDStr = classIter.substring($.fn.treeTable.defaults.childPrefix.length + 2)
                parentID = parseInt(parentIDStr)
                if isNaN(parentID) == true
                    parentID = -1

        if parentID != -1
            parentLesson = lessons[parentID]
            childLessonIndex = parentLesson.childLessons.indexOf(uiID)
            parentLesson.childLessons.splice(childLessonIndex, 1)
            window.modifiedLessons[parentID] = parentLesson

    #Javascript side
    lessonToRemove = lessons[uiID]

    if confirm("Are you sure you wish to delete lesson '" + lessonToRemove.name + "'?")
        trRow = $("#" + rowId)

        removeLessonAndChildren(uiID, lessonToRemove)
        removeLessonChildFromParent(uiID, trRow)

        #Table bits
        removeTableRow(trRow)

        for lesson in window.newLessons
            if lesson.lessonID == uiID
                window.newLessons[uiID] = undefined

        #updateDueValues()


getParentLesson = (lesson) ->
    trRow = $("#l_" + lesson.lessonID)
    parentID = -1
    parentLesson = null
    trRowClasses = trRow.attr("class").split(" ")
    for classIter in trRowClasses
        if classIter.indexOf($.fn.treeTable.defaults.childPrefix + "l_") == 0
            parentIDStr = classIter.substring($.fn.treeTable.defaults.childPrefix.length + 2)
            parentID = parseInt(parentIDStr)
            if isNaN(parentID) == true
                parentID = -1

    if parentID != -1
        parentLesson = lessons[parentID]

    return parentLesson


window.newLessonCount = 0
window.handleAddChildLesson = (uiID, rowId) ->
    determineChildDepth = (childLesson) ->
        curLesson = childLesson
        curDepth = 0
        while (curLesson != null)
            curDepth++
            curLesson = getParentLesson(curLesson)

        return curDepth - 1

    #Add new lesson to javascript object
    newID = lessons.length
    for lessonIter in lessons
        if (typeof lessonIter != "undefined") and (lessonIter.lessonID > newID)
            newID = lessonIter.lessonID + 1

    window.newLessonCount++
    newChildLesson = {
            uniqueLessonID: -1,
            lessonID: newID,
            name: "New Lesson",
            category: "",
            selected: true,
            childLessons: [],
            entries: []
        }

    window.newLessons[newID] = newChildLesson
        
    parentLesson = lessons[uiID]
    parentLesson.childLessons.push(newID)
    lessons.push(newChildLesson)
    newChildLesson.selected = parentLesson.selected

    window.modifiedLessons[uiID] = parentLesson

    #Add new lesson to table
    childDepth = determineChildDepth(parentLesson) + 1
    childDepthOffset = childDepth * $.fn.treeTable.defaults.indent

    parentRow = $("#" + rowId)
    if parentRow.hasClass("parent") == false
        parentRow.addClass("parent")
        parentRow.addClass("collapsed")
        #parentDepthOffset = (childDepth - 1) * $.fn.treeTable.defaults.indent
        expanderSpan = '<span style="padding-left: ' + $.fn.treeTable.defaults.indent + 'px;" class="expander"></span>'
        parentRow.children("td:first-child").prepend(expanderSpan)
        $(parentRow.children("td")[0].firstChild).click(-> parentRow.toggleBranch())
        parentRow.toggleBranch()

    hiddenClass = ""
    if parentRow.hasClass("collapsed")
        hiddenClass = "ui-helper-hidden"

    tableEntry = '<tr id="l_' + newID + '" class="child-of-' + rowId + ' initialized ' + hiddenClass + '">' +
                    '<td style="padding-left: ' + childDepthOffset + 'px; ">' +
                    '<span class="quizTreeRow">' +
                    '<div class="hideMe quizTreeControlRow">' +

                    #Add bits
                    '&nbsp;<span class="controlAction" onclick="handleAddChildLesson(\'' + newID + '\', \'l_' + newID + '\')"' +
                    '><img src="@{StaticR img_addChildLesson_png}"/><span class="catooltip">Add Child Lesson</span></span>&nbsp;' +

                    #remove bits
                    '&nbsp;<span class="controlAction" onclick="handleRemoveLesson(\'' + newID + '\', \'l_' + newID + '\')"' +
                    '><img src="@{StaticR img_deleteLesson_png}"/><span class="catooltip">Delete Lesson</span></span>&nbsp;' +
                    
                    #rename bits
                    '&nbsp;<span class="controlAction" onclick="handleRenameLesson(\'' + newID + '\', \'l_' + newID + '\')"' +
                    '><img src="@{StaticR img_renameLesson_png}"/><span class="catooltip">Rename Lesson</span></span>&nbsp;' +

                    #edit category
                    '&nbsp;<span class="controlAction" onclick="handleEditLessonCategory(\'' + newID + '\', \'l_' + newID + '\')"' +
                    '><img src="@{StaticR img_editCategory_png}"/><span class="catooltip">Edit Lesson Category</span></span>&nbsp;' +

                    '</div>' + #quizTreeControlRow
                    '<span class="quizTreeRowText" uiID="' + newID + '" rowId="l_' + newID + '">' +
                    newChildLesson.name +
                    '</span></span></td>' +
                    
                    '<td style="text-align: center; vertical-align: middle;">' +
                    '<input class="selection_checkbox" type="checkbox" name="selected" checked="' + newChildLesson.selected + '" onclick="handleSelectedCheckChange(this, \'' + newID + '\', \'l_' + newID + '\')"/>' +
                    '</td>' +
                    '<td class="numDue1">0</td><td class="numDue2">0</td>' +
                    '</tr>'
       
    parentRow.after(tableEntry)

    setupTreeHoverMouseDown()
    

window.createUILessonMap = (kvtml) ->
    retMap = []

    createUILessonMapHelper = (lesson) ->
        for childLesson in lesson.lessons
            createUILessonMapHelper childLesson
            childLesson.parentLesson = lesson

        retMap[lesson.uiID] = lesson

    for childLesson in kvtml.lessons
        childLesson.parentLesson = null
        createUILessonMapHelper childLesson

    return retMap


quizTreeRowTextMouseDown = (spanNode) ->
    $(".quizTreeRowSelected").removeClass("quizTreeRowSelected")
    spanNode.addClass("quizTreeRowSelected")
    
    $(".quizInfo").hide()
    $(".entryRows").show()

    lessonName = spanNode.text()
    $("#contentTitle").text("Lesson Entries for " + lessonName)

    #Clear current entries
    $("#entryTree tbody tr").remove()

    #Add new entries
    uiID = spanNode.attr("uiid")
    selectedLesson = lessons[uiID]
    $("#entryRowButton").attr("onclick", 'handleAddNewEntryRow(\'' + uiID + '\')')

    newRowText = "<tr>"
    numLanguages = 2
    for language in [0...numLanguages]
        newRowText = newRowText + "<th>" + quiz.language[language] + "</th>"
    newRowText = newRowText + "</tr>"
    $("#entryTree tbody").append(newRowText)

    ids = []
    for entryID in selectedLesson.entries
        entry = entries[entryID]
        newRowText = '<tr id="entryRow_' + entryID + '">'

        for language in [0...numLanguages]
            editableId = "editID_" + language + "_" + entryID
            newRowText = newRowText + '<td class="editable" id="' + editableId + '" language="' + language + '" entryID="' + entryID + '">' + getEntryTranslation(entry, language).text + "</td>"
            ids.push(editableId)

        if isEditableQuiz
            newRowText = newRowText + '<td><span class="delEntry" onclick="deleteEntry(\'' + entryID + '\', \'' + uiID + '\')"><img src="@{StaticR img_remove_icon_png}"/></span></td></tr>'

        $("#entryTree tbody").append(newRowText)

    if isEditableQuiz
        for id in ids
            $('#' + id).editable(createTreeEditOptions(id))
    

getEntryTranslation = (entry, language) ->
    if (language == 0)
        {
            count: entry.count1,
            date: entry.date1,
            errorCount: entry.errorCount1,
            grade: entry.grade1,
            text: entry.text1
        }
    else
        {
            count: entry.count2,
            date: entry.date2,
            errorCount: entry.errorCount2,
            grade: entry.grade2,
            text: entry.text2
        }


createTreeEditOptions = (id) ->
    treeEditOptions = {
                        onSubmit : (content) -> handleEditRowSubmit(content, id)
    }
    return treeEditOptions


window.deletedEntries = []
window.deleteEntry = (idStr, uiID) ->
    editedTR = $("#entryRow_" + idStr)
    editedTR.remove()

    id = parseInt(idStr, 10)
    window.deletedEntries.push(id)

    selectedLesson = lessons[uiID]
    entryIndex = selectedLesson.entries.indexOf(id)
    selectedLesson.entries.splice(entryIndex, 1)

    window.modifiedLessons[uiID] = selectedLesson

    for entry in window.newEntries
        if entry.entryID == uiID
            window.newEntries[uiID] = undefined

    #updateDueValues()


handleEditRowSubmit = (content, id) ->
    editedTDNode = $("#" + id)

    if (content.current.length == 0)
        editedTDNode.text(content.previous)
        return

    if (content.current == content.previous)
        return

    language = parseInt(editedTDNode.attr("language"))
    entryID = parseInt(editedTDNode.attr("entryID"))

    entry = entries[entryID]
    window.modifiedEntries[entryID] = entry

    if (language == 0)
        entry.text1 = content.current
    else
        entry.text2 = content.current


window.handleAddNewEntryRow = (uiID) ->
    selectedLesson = lessons[uiID]
    newEntryID = entries.length

    for entryIter in entries
        if (typeof lessonIter != "undefined") and (entryIter.entryID > newEntryID)
            newEntryID = entryIter.entryID + 1

    newEntry = {
            uniqueEntryID: -1,
            entryID: newEntryID,
            count1: 0,
            count2: 0,
            grade1: 0,
            grade2: 0,
            errorCount1: 0,
            errorCount2: 0,
            date1: "2012-01-01 01:01:01",
            date2: "2012-01-01 01:01:01",
            text1: "XXXXX",text2: "XXXX"
        }

    entries[newEntryID] = newEntry
    window.newEntries[newEntryID] = newEntry

    window.modifiedLessons[uiID] = selectedLesson

    newRowText = '<tr id="entryRow_' + newEntry.entryID + '">'

    numLanguages = 2

    ids = []
    for language in [0...numLanguages]
        editableId = "editID_" + language + "_" + newEntry.entryID
        newRowText = newRowText + '<td class="editable" id="' + editableId + '" language="' + language + '" entryID="' + newEntry.entryID + '">' + getEntryTranslation(newEntry, language).text + "</td>"
        ids.push(editableId)

    newRowText = newRowText + '<td><span class="delEntry" onclick="deleteEntry(\'' + newEntry.entryID + '\', \'' + uiID + '\')"><img src="@{StaticR img_remove_icon_png}"/></span></td></tr>'

    $("#entryTree tbody").append(newRowText)

    if isEditableQuiz
        for id in ids
            $('#' + id).editable(createTreeEditOptions(id))

    selectedLesson.entries.push(newEntry.entryID)

    #updateDueValues()


handleEditQuizTitle = (content) ->
    if (content.current.length > 0)
        quiz.title = content.current
        $("#sideBarQuizTitle").text(content.current)
    else
        $("#quizInfoTitle").text(content.previous)


handleEditLanguage = (language, content) ->
    quiz.language[language] = content.current


handleEditLeitner = (level, content) ->
    delayInt = parseInt(content.current)
    if (content.current.length > 0) and (isNaN(delayInt) == false) and (delayInt < 255)
        quiz.levels[level] = delayInt
        $("#leitner_" + (level + 1)).text(delayInt)
    else
        $("#leitner_" + (level + 1)).text(content.previous)


window.handleOnQuizInfoSpanClick = ->
    $(".entryRows").hide()
    $(".quizInfo").show()

    $("#quizInfoTitle").text(quiz.title)

    $("#quizInfoLanguage1Name").text(quiz.language[0])
    $("#quizInfoLanguage2Name").text(quiz.language[1])

    for level in [1..20]
        $("#leitner_" + level).text(quiz.levels[level - 1])

    $("#quizInfoTitle").editable({onSubmit : (content) -> handleEditQuizTitle(content)})
    $("#quizInfoLanguage1Name").editable({onSubmit : (content) -> handleEditLanguage(0, content)})
    $("#quizInfoLanguage2Name").editable({onSubmit : (content) -> handleEditLanguage(1, content)})

    $("#leitner_1").editable({onSubmit : (content) -> handleEditLeitner(0, content)})
    $("#leitner_2").editable({onSubmit : (content) -> handleEditLeitner(1, content)})
    $("#leitner_3").editable({onSubmit : (content) -> handleEditLeitner(2, content)})
    $("#leitner_4").editable({onSubmit : (content) -> handleEditLeitner(3, content)})
    $("#leitner_5").editable({onSubmit : (content) -> handleEditLeitner(4, content)})
    $("#leitner_6").editable({onSubmit : (content) -> handleEditLeitner(5, content)})
    $("#leitner_7").editable({onSubmit : (content) -> handleEditLeitner(6, content)})
    $("#leitner_8").editable({onSubmit : (content) -> handleEditLeitner(7, content)})
    $("#leitner_9").editable({onSubmit : (content) -> handleEditLeitner(8, content)})
    $("#leitner_10").editable({onSubmit : (content) -> handleEditLeitner(9, content)})
    $("#leitner_11").editable({onSubmit : (content) -> handleEditLeitner(10, content)})
    $("#leitner_12").editable({onSubmit : (content) -> handleEditLeitner(11, content)})
    $("#leitner_13").editable({onSubmit : (content) -> handleEditLeitner(12, content)})
    $("#leitner_14").editable({onSubmit : (content) -> handleEditLeitner(13, content)})
    $("#leitner_15").editable({onSubmit : (content) -> handleEditLeitner(14, content)})
    $("#leitner_16").editable({onSubmit : (content) -> handleEditLeitner(15, content)})
    $("#leitner_17").editable({onSubmit : (content) -> handleEditLeitner(16, content)})
    $("#leitner_18").editable({onSubmit : (content) -> handleEditLeitner(17, content)})
    $("#leitner_19").editable({onSubmit : (content) -> handleEditLeitner(18, content)})
    $("#leitner_20").editable({onSubmit : (content) -> handleEditLeitner(19, content)})

    $("#maxEntriesPerSession").editable({onSubmit : (content) -> handleEditMaxEntriesPerSession(content)})
    $("#minQuestionLocality").editable({onSubmit : (content) -> handleEditMinQuestionLocality(content)})
    $("#maxQuestionLocality").editable({onSubmit : (content) -> handleEditMaxQuestionLocality(content)})
    $("#numRepeatUntilAccepted").editable({onSubmit : (content) -> handleEditNumRepeatUntilAccepted(content)})
    $("#fontSizeEdit").editable({onSubmit : (content) -> handleEditFontSize(content)})

    $("#maxEntriesPerSession").text(quiz.maxSessionEntries)
    $("#minQuestionLocality").text(quiz.minQLocality)
    $("#maxQuestionLocality").text(quiz.maxQLocality)
    $("#numRepeatUntilAccepted").text(quiz.numRepeatUntilAccepted)
    $("#fontSizeEdit").text(quiz.fontSize)

    $("#useRealLeitner").attr('checked', quiz.realLeitner)
    $("#showWordCycle").attr('checked', quiz.showWordCycle)
    $("#resetCorrectCount").attr('checked', quiz.resetCountOnWrong)
    $("#flashcardModeRandom").attr('checked', quiz.randomFlashcardMode)


window.handleDeleteQuiz = ->
    if confirm("Are you sure you wish to delete this quiz?")
        $(".saveChangesButton").attr("disabled", true)

        $.ajax({
            url: "@{DeleteQuizBaseR}/" + window.quizID,
            type: "POST",
            data : { },
            dataType : "json",
            async : true,
            cache : false,

            success : (result, textStatus, jqXHR) ->
                $(".saveChangesButton").attr("disabled", false)
                if result.succeeded == true
                    window.location.href="@{FrontDoorR}"
                else
                    alert("Fail on delete: " + result.message)

            error : (jqXHR, textStatus, errorThrown) ->
                $(".saveChangesButton").attr("disabled", false)
                alert("Error on quiz deletion: " + textStatus)
        })


window.handleOnSaveChanges = ->
    modifiedEntryArray = new Array()
    for k,entryIter of window.modifiedEntries
        if (typeof entryIter != "undefined")
            modifiedEntryArray.push(entryIter)

    newEntryArray = new Array()
    for k,entryIter of window.newEntries
        if (typeof entryIter != "undefined")
            newEntryArray.push(entryIter)

    modifiedLessonArray = new Array()
    for k,lessonIter of window.modifiedLessons
        if (typeof lessonIter != "undefined")
            modifiedLessonArray.push(lessonIter)

    newLessonArray = new Array()
    for k,lessonIter of window.newLessons
        if (typeof lessonIter != "undefined")
            newLessonArray.push(lessonIter)

    bundledKVTML = {
                    quizID : quizID,
                    version: quiz.version,
                    title : quiz.title,
                    language1: quiz.language[0],
                    language2: quiz.language[1],
                    levels: quiz.levels,
                    rootLesson: quiz.rootLesson,
                    realLeitner: quiz.realLeitner,
                    minQLocality: quiz.minQLocality,
                    maxQLocality: quiz.maxQLocality,
                    showWordCycle: quiz.showWordCycle,
                    numRepeatUntilAccepted: quiz.numRepeatUntilAccepted,
                    resetCountOnWrong: quiz.resetCountOnWrong,
                    randomFlashcardMode: quiz.randomFlashcardMode,
                    maxSessionEntries: quiz.maxSessionEntries,
                    fontSize: quiz.fontSize,

                    modifiedEntries : modifiedEntryArray,
                    newEntries : newEntryArray,
                    modifiedLessons: modifiedLessonArray,
                    newLessons: newLessonArray
    }

    $(".saveChangesButton").attr("disabled", true)

    $.ajax({
        url: "@{EditQuizSaveChangesR}",
        type: "POST",
        data :  {
                    quiz: JSON.stringify(bundledKVTML)
                },
        dataType : "json",
        async : true,
        cache : false,

        success : (result, textStatus, jqXHR) ->
            $(".saveChangesButton").attr("disabled", false)
            if result.succeeded == true
                window.modifiedEntries = new Object()
                window.newEntries = new Object()
                window.modifiedLessons = new Object()
                window.newLessons = new Object()
            else
                alert("Fail on save: " + result.message)

        error : (jqXHR, textStatus, errorThrown) ->
            $(".saveChangesButton").attr("disabled", false)
            alert("Error on save: " + textStatus)
    })


window.handleSelectedCheckChange = (checkbox, uiID, rowId) ->
    isChecked = checkbox.checked

    childrenOf = (node) ->
        $(node).siblings("tr." + $.fn.treeTable.defaults.childPrefix + node.attr("id"))

    updateTableRowSelectionState = (node) ->
        childrenOf(node).each( (index, value) -> updateTableRowSelectionState($(this)) )
        checkboxNode = $(node).find(".selection_checkbox")
        checkboxNode.attr('checked', isChecked)

    updateTableRowSelectionStateJS = (lesson) ->
        lesson.selected = isChecked

        window.modifiedLessons[lesson.lessonID] = lesson

        for childLessonIndex in lesson.childLessons
            childLesson = lessons[childLessonIndex]
            updateTableRowSelectionStateJS childLesson

        return null

    #Javascript side
    lessonSelected = lessons[uiID]
    updateTableRowSelectionStateJS lessonSelected

    #Table bits
    updateTableRowSelectionState($("#" + rowId))




window.handleEditMaxEntriesPerSession = (content) ->
    num = parseInt(content.current)
    if (content.current.length > 0) and (isNaN(num) == false)
        quiz.maxSessionEntries = num
        $("#maxEntriesPerSession").text(num)
    else
        $("#maxEntriesPerSession").text(content.previous)


window.handleEditMinQuestionLocality = (content) ->
    num = parseInt(content.current)
    if (content.current.length > 0) and (isNaN(num) == false)
        quiz.minQLocality = num
        $("#minQuestionLocality").text(num)
    else
        $("#minQuestionLocality").text(content.previous)


window.handleEditMaxQuestionLocality = (content) ->
    num = parseInt(content.current)
    if (content.current.length > 0) and (isNaN(num) == false)
        quiz.maxQLocality = num
        $("#maxQuestionLocality").text(num)
    else
        $("#maxQuestionLocality").text(content.previous)


window.handleEditNumRepeatUntilAccepted = (content) ->
    num = parseInt(content.current)
    if (content.current.length > 0) and (isNaN(num) == false)
        quiz.numRepeatUntilAccepted = num
        $("#numRepeatUntilAccepted").text(num)
    else
        $("#numRepeatUntilAccepted").text(content.previous)


window.handleEditFontSize = (content) ->
    num = parseFloat(content.current)
    if (content.current.length > 0) and (isNaN(num) == false)
        number = new Number(num)
        quiz.fontSize = number.toFixed(2)
        $("#fontSizeEdit").text(quiz.fontSize)
    else
        $("#fontSizeEdit").text(content.previous)


window.handleRealLeitnerCheckChange = (checkbox) ->
    quiz.realLeitner = checkbox.checked


window.handleShowWordCycleCheckChange = (checkbox) ->
    quiz.showWordCycle = checkbox.checked


window.handleResetCorrectCountCheckChange = (checkbox) ->
    quiz.resetCountOnWrong = checkbox.checked


window.handleFlashcardModeRandomCheckChange = (checkbox) ->
    quiz.randomFlashcardMode = checkbox.checked


updateDueValues = ->
    updateLessonDueValues = (lesson) ->
        totalDue1 = 0
        totalDue2 = 0
        for childLessonIndex in lesson.childLessons
            childLesson = lessons[childLessonIndex]
            updateLessonDueValues(childLesson)
            totalDue1 += childLesson.numDue1
            totalDue2 += childLesson.numDue2

        for entryIndex in lesson.entries
            entry = entries[entryIndex]
            if entry.isDue1 == true
                totalDue1++
            if entry.isDue2 == true
                totalDue2++

        lesson.numDue1 = totalDue1
        lesson.numDue2 = totalDue2

    rootLesson = lessons[quiz.rootLesson]
    updateLessonDueValues(rootLesson)

    # Somewhere the numbers got flipped
    for lesson in lessons
        isDue1TDNode = $("#l_" + lesson.lessonID + " .numDue1")
        if isDue1TDNode?
            isDue1TDNode.text(lesson.numDue2)

        isDue2TDNode = $("#l_" + lesson.lessonID + " .numDue2")
        if isDue2TDNode?
            isDue2TDNode.text(lesson.numDue1)








