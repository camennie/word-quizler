
###
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.
###


daysBetween = (date1, date2) ->
    difference = date2 - date1

    msPerDay = 1000 * 60 * 60 * 24

    #date1_ms = date1.getTime()
    #date2_ms = date2.getTime()

    #difference = Math.abs(date1_ms - date2_ms)
    
    numDays = Math.round(difference / msPerDay)

    #console.log "db: " + date1 + " -- " + date2
    #console.log "numDays: " + numDays

    return numDays


isEntryDue = (entry, translationID) ->
    altTranslationID = translationID #(translationID + 1) % 2 #somewhere we screwed this up..

    #tmpdate = Date.parseExact("2012-01-01 01:01:01", "yyyy-MM-dd HH:mm:ss")

    date1Str = entry.date1.slice(0, -9)
    date2Str = entry.date2.slice(0, -9)
    #date1 = Date.parseExact(date1Str, "yyyy-MM-dd")
    #date2 = Date.parseExact(date2Str, "yyyy-MM-dd")

    if translationID == 0
        lastDate = Date.parseExact(date1Str, "yyyy-MM-dd")
    else
        lastDate = Date.parseExact(date2Str, "yyyy-MM-dd")

    currentDate = Date.today()
    diffDays = daysBetween(lastDate, currentDate)

    if translationID == 0
        entryGrade = entry.grade1
    else
        entryGrade = entry.grade2

    entryGrade0 = entry.grade1
    entryGrade1 = entry.grade2

    #console.log "ied: " + entry.translation[0].text + " -- " + translationID + " -- " + date1 + " -- " + date2 + " --diffDays: " + diffDays + "--entryGrade0: " + entryGrade0 + "--entryGrade1: " + entryGrade1

    return diffDays >= quiz.levels[entryGrade]


window.determineDueEntries = ->
    for entry in entries
        if typeof entry != "undefined"
            if isEntryDue(entry, 0) == true
                entry.isDue1 = true

            if isEntryDue(entry, 1) == true
                entry.isDue2 = true

    return true







