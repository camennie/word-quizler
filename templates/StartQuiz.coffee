
###
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.
###


$(document).ready ->
    window.determineDueEntries()
    window.quizGlobals = createQuizGlobals()

    window.quizGlobals.lang[0] = 0
    window.quizGlobals.lang[1] = 1

    if startLanguage == 1
        window.quizGlobals.lang[0] = 1
        window.quizGlobals.lang[1] = 0

    if startLanguage == 99
        window.quizGlobals.bothMode = true

    $("#quizTitle").text(quiz.title)

    $("#exitButton").click(handleStop)

    $("#cheatButton").click(handleCheat)
    $("#failButton").click(handleFail)
    $("#verifyButton").click(handleVerify)

    $("#fromText").css("fontSize", quiz.fontSize + "em")
    $("#answerInput").css("fontSize", quiz.fontSize + "em")

    $("#answerInput").keydown(handleInputKeypress)

    statusSize = quiz.fontSize / 4
    if statusSize < 1
        statusSize = 1

    $("#statusText").css("fontSize", statusSize + "em")
    $("#categoryText").css("fontSize", statusSize + "em")

    startQuiz()


createQuizGlobals = ->
    quizGlobals =
        lang : []
        bothMode : false
        quizEntries : []
        entryLessonMap : new Object()
        currentlyShowingAnswer : false
        currentlyShowingCorrectAnswer : false
        currentlyShowingWrongAnswer : false
        origQuestionCount : 0

    quizGlobals.quizEntries[0] = []
    quizGlobals.quizEntries[1] = []
    quizGlobals.lang[0] = 0
    quizGlobals.lang[1] = 1

    return quizGlobals


handleInputKeypress = (e) ->
    if e.keyCode == 13
        handleVerify()
        return false

    if e.altKey == false
        return true
    else
        if e.keyCode == 75 #k
            handleCheat()
            return false

        if e.keyCode == 83 #s
            handleFail()
            return false

    return true


handleStop = ->
    answered = window.quizGlobals.origQuestionCount
    $("#progressbar").attr("value", answered)
    $("#progressCount").text(answered + " / " + window.quizGlobals.origQuestionCount)

    quizResultStore =
        curQuizID : quizID
        entries : []

    for direction in [0..1]
        for quizEntry in window.quizGlobals.origQuizEntries[direction]
            entryInfo = {
                    uniqueEntryID: quizEntry.entry.uniqueEntryID
                    grade1: quizEntry.entry.grade1
                    grade2: quizEntry.entry.grade2
                    date1: quizEntry.entry.date1
                    date2: quizEntry.entry.date2
            }

            quizResultStore.entries.push(entryInfo)

    json_results = $.toJSON(quizResultStore)
    $.jStorage.deleteKey("pendingResults")
    $.jStorage.set("pendingResults", json_results)

    #console.log $.toJSON(quizResultStore)

    $("#cheatButton").attr("disabled", true)
    $("#failButton").attr("disabled", true)
    $("#verifyButton").attr("disabled", true)

    window.location.href="@{SessionOpenedR quizBaseId}"


createQuizEntry = (entryBase, index) ->
    quizEntry =
        entry : entryBase
        answeredIncorrectly : false
        correctAnswers : 0
        index : index

    return quizEntry


collectQuizEntries = (lessonId, parentCategory, direction) ->
    lesson = lessons[lessonId]

    if lesson.category == ""
        lesson.category = parentCategory

    if lesson.selected == true
        for entryIndex in lesson.entries
            entry = entries[entryIndex]

            isDue = false
            if (entry.isDue1 == true) and (direction == 0)
                isDue = true
            if (entry.isDue2 == true) and (direction == 1)
                isDue = true

            if isDue == true
                quizEntry = createQuizEntry(entry, entryIndex)
                window.quizGlobals.quizEntries[direction].push(quizEntry)
                window.quizGlobals.entryLessonMap["a" + quizEntry.index.toString()] = lesson

    for childLessonId in lesson.childLessons
        collectQuizEntries(childLessonId, lesson.category, direction)

    return null


getEntryTranslation = (entry, language) ->
    if (language == 0)
        {
            count: entry.count1,
            date: entry.date1,
            errorCount: entry.errorCount1,
            grade: entry.grade1,
            text: entry.text1
        }
    else
        {
            count: entry.count2,
            date: entry.date2,
            errorCount: entry.errorCount2,
            grade: entry.grade2,
            text: entry.text2
        }


shuffleArray = (array) ->
    top = array.length - 1

    if (top > 0)
        while (top >= 0)
            current = Math.floor(Math.random() * (top + 1))
            tmp = array[current]
            array[current] = array[top]
            array[top] = tmp
            --top

    return array


setCategoryText = (nextEntry) ->
    entryLesson = window.quizGlobals.entryLessonMap["a" + nextEntry.index.toString()]

    #console.log "entryLesson: " + entryLesson.name + " -- " + nextEntry.entry.translation[0].text

    category = entryLesson.category

    $("#categoryText").text(category)

    if category.length > 0
        $("#categoryText").removeClass("hiddenClass")
    else
        $("#categoryText").addClass("hiddenClass")


createQuiz = ->
    window.quizGlobals.quizEntries = []
    window.quizGlobals.origQuizEntries = []

    window.quizGlobals.quizEntries[0] = []
    window.quizGlobals.origQuizEntries[0] = []
    window.quizGlobals.quizEntries[1] = []
    window.quizGlobals.origQuizEntries[1] = []

    startLang = window.quizGlobals.lang[0]
    endLang = window.quizGlobals.lang[0]

    if window.quizGlobals.bothMode == true
        startLang = 0
        endLang = 1

    for direction in [startLang..endLang]
        window.quizGlobals.quizEntries[direction] = []
        window.quizGlobals.origQuizEntries[direction] = []

        collectQuizEntries(0, "", direction)

        window.quizGlobals.quizEntries[direction] = shuffleArray(window.quizGlobals.quizEntries[direction])

        if (quiz.maxSessionEntries > 0) and (window.quizGlobals.quizEntries[direction].length > quiz.maxSessionEntries)
            window.quizGlobals.quizEntries[direction] = window.quizGlobals.quizEntries[direction].slice(0, quiz.maxSessionEntries)

        for quizEntry in window.quizGlobals.quizEntries[direction]
            window.quizGlobals.origQuizEntries[direction].push(quizEntry)

    #console.log "quiz entries: " + window.quizGlobals.quizEntries.length


startQuiz = ->
    createQuiz()

    if window.quizGlobals.bothMode == true
        window.quizGlobals.lang[0] = parseInt(Math.round(Math.random()))
        window.quizGlobals.lang[1] = 1 - window.quizGlobals.lang[0]

    if (window.quizGlobals.quizEntries[0].length == 0) and (window.quizGlobals.quizEntries[1].length == 0)
        $("#cheatButton").attr("disabled", true)
        $("#failButton").attr("disabled", true)
        $("#verifyButton").attr("disabled", true)

        $("#fromText").text("")
        $("#answerInput").text("")
        $("#answerInput").attr("value", "")
        $("#statusText").addClass("hiddenClass")
        $("#categoryText").addClass("hiddenClass")

        $("#cycleCount").text("")
        $("#progressCount").text("")

        alert("Nothing due")

        window.location.href="@{SessionOpenedR quizBaseId}"
        return

    $("#progressbar").attr("value", 0)
    $("#progressbar").attr("max", window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length)
    $("#progressCount").text("1 / " + window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length)

    window.quizGlobals.currentlyShowingAnswer = false
    window.quizGlobals.currentlyShowingCorrectAnswer = false
    window.quizGlobals.currentlyShowingWrongAnswer = false
    window.quizGlobals.origQuestionCount = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length

    if window.quizGlobals.bothMode == true
        window.quizGlobals.origQuestionCount += window.quizGlobals.quizEntries[window.quizGlobals.lang[1]].length

    setNextQuestion()


setNextQuestion = ->
    window.quizGlobals.currentlyShowingAnswer = false
    window.quizGlobals.currentlyShowingCorrectAnswer = false
    window.quizGlobals.currentlyShowingWrongAnswer = false

    $("#answerInput").text("")
    $("#answerInput").attr("value", "")
    $("#statusText").addClass("hiddenClass")
    $("#categoryText").addClass("hiddenClass")

    $("#answerInput").css('color', 'black')

    if (window.quizGlobals.quizEntries[0].length == 0) and (window.quizGlobals.quizEntries[1].length == 0)
        $("#fromText").addClass("hiddenClass")
        handleStop()
        return

    if window.quizGlobals.bothMode == true
        window.quizGlobals.lang[0] = parseInt(Math.round(Math.random()))
        window.quizGlobals.lang[1] = 1 - window.quizGlobals.lang[0]

    nextEntry = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]][0]
    
    moveFirstEntry = true
    if nextEntry.correctAnswers >= quiz.numRepeatUntilAccepted
        handleFinishedEntry(nextEntry)
        #console.log "finished with " + JSON.stringify(nextEntry)
        #console.log ""
        window.quizGlobals.quizEntries[window.quizGlobals.lang[0]] = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].slice(1)
        moveFirstEntry = false

    if (moveFirstEntry == true) and (window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length > 1)
        window.quizGlobals.quizEntries[window.quizGlobals.lang[0]] = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].slice(1)
        repositionFirstEntry(nextEntry)

    if window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length == 0
        $("#fromText").addClass("hiddenClass")
        handleStop()
        return

    $("#answerInput").focus()

    nextEntry = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]][0]

    $("#cycleCount").text((nextEntry.correctAnswers + 1) + " / " + quiz.numRepeatUntilAccepted)

    answered = window.quizGlobals.origQuestionCount - window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length
    $("#progressbar").attr("value", answered)

    $("#progressCount").text(answered + " / " + window.quizGlobals.origQuestionCount)

    $("#fromText").text(getEntryTranslation(nextEntry.entry, window.quizGlobals.lang[1]).text)
    window.quizGlobals.currentAnswer = getEntryTranslation(nextEntry.entry, window.quizGlobals.lang[0]).text

    setCategoryText(nextEntry)


handleFinishedEntry = (curEntry) ->
    if window.quizGlobals.lang[0] == 0
        curEntry.entry.count1++

        curDate = Date.today()
        curEntry.entry.date1 = curDate.toString("yyyy-MM-dd") + " 01:01:01"

        if curEntry.answeredIncorrectly == true
            curEntry.entry.grade1 = 0
            curEntry.entry.errorCount1++
        else
            if curEntry.entry.grade1 < (quiz.levels.length - 1)
                curEntry.entry.grade1++
    else
        curEntry.entry.count2++

        curDate = Date.today()
        curEntry.entry.date2 = curDate.toString("yyyy-MM-dd") + " 01:01:01"

        if curEntry.answeredIncorrectly == true
            curEntry.entry.grade2 = 0
            curEntry.entry.errorCount2++
        else
            if curEntry.entry.grade2 < (quiz.levels.length - 1)
                curEntry.entry.grade2++

    #console.log "entry grade is: " + curEntry.entry.translation[window.quizGlobals.lang[0]].grade + " -- " + curEntry.entry.translation[window.quizGlobals.lang[0]].date + " -- " + window.quizGlobals.lang[0]


repositionFirstEntry = (curEntry) ->
    quizLength = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].length

    minLocality = parseInt quiz.minQLocality
    if minLocality > quizLength
        minLocality = 1

    if minLocality < 0
        minLocality = 1
    
    maxLocality = parseInt quiz.maxQLocality
    if maxLocality > quizLength
        maxLocality = quizLength
    
    if maxLocality < 0
        maxLocality = quizLength
    
    factor = Math.random()
    preFloor = factor * (maxLocality - minLocality + 1)
    nextPos = Math.floor(preFloor) + minLocality

    if nextPos > quizLength
        nextPos = quizLength

    #console.log "nextPos is: " + nextPos + " min: " + minLocality + "  --  max: " + maxLocality + "  --  factor: " + factor + "  --  " + preFloor
    
    window.quizGlobals.quizEntries[window.quizGlobals.lang[0]].splice(nextPos, 0, curEntry)


handleCheat = ->
    if window.quizGlobals.currentlyShowingCorrectAnswer == true
        nextEntry = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]][0]
        nextEntry.correctAnswers++

        setNextQuestion()
        return

    $("#answerInput").css('color', '#00ff00')

    $("#statusText").removeClass("hiddenClass")
    $("#statusText").text("CORRECT: " + window.quizGlobals.currentAnswer)
    window.quizGlobals.currentlyShowingAnswer = true
    window.quizGlobals.currentlyShowingCorrectAnswer = true
    window.quizGlobals.currentlyShowingWrongAnswer = false

    if $("#answerInput").attr("value").length == 0
        $("#answerInput").attr("value", window.quizGlobals.currentAnswer)


handleFail = ->
    if window.quizGlobals.currentlyShowingWrongAnswer == true
        nextEntry = window.quizGlobals.quizEntries[window.quizGlobals.lang[0]][0]
        nextEntry.answeredIncorrectly = true
        nextEntry.correctAnswers = 0
        setNextQuestion()
        return

    $("#answerInput").css('color', '#ff0000')

    $("#statusText").removeClass("hiddenClass")
    $("#statusText").text("INCORRECT: " + window.quizGlobals.currentAnswer)
    window.quizGlobals.currentlyShowingAnswer = true
    window.quizGlobals.currentlyShowingCorrectAnswer = false
    window.quizGlobals.currentlyShowingWrongAnswer = true

    if $("#answerInput").attr("value").length == 0
        $("#answerInput").attr("value", window.quizGlobals.currentAnswer)


handleVerify = ->
    curTranslation = $("#answerInput").attr("value").toLowerCase()
    if curTranslation.length == 0
        return

    #if window.quizGlobals.currentlyShowingAnswer == true
    #    setNextQuestion()
    #    return

    currentAnswerLower = window.quizGlobals.currentAnswer.toLowerCase()

    curTranslationBits = $.map(curTranslation.split(/[,、]/), (val, index) -> return $.trim(val)).sort()
    curAnswerBits = $.map(currentAnswerLower.split(/[,、]/), (val, index) -> return $.trim(val)).sort()

    allSame = true
    if curTranslationBits.length != curAnswerBits.length
        allSame = false
        #console.log 'out 1'
    else
        for index in [0...curTranslationBits.length]
            if curTranslationBits[index] != curAnswerBits[index]
                allSame = false
                #console.log 'out 2: ' + index + '/' + curTranslationBits.length + ' -- "' + curTranslationBits[index] + '" -- "' + curAnswerBits[index] + '"'
                #console.log 'curTranslationBits: ' + $.toJSON(curTranslationBits)
                #console.log 'curAnswerBits: ' + $.toJSON(curAnswerBits)
                break

    if allSame == true
        handleCheat()
    else
        handleFail()







