{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Main where

import Import
import Settings
import Yesod.Logger (defaultDevelopmentLogger)
import Yesod.Default.Config
import Yesod.Test
import Application (makeFoundation)

import HomeTest

main :: IO a
main = do
    conf <- loadConfig $ (configSettings Testing) { csParseExtra = parseExtra }
    logger <- defaultDevelopmentLogger
    foundation <- makeFoundation conf logger
    app <- toWaiAppPlain foundation
    runTests app (connPool foundation) homeSpecs
