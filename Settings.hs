
{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



-- | Settings are centralized, as much as possible, into this file. This
-- includes database connection settings, static file locations, etc.
-- In addition, you can configure a number of different aspects of Yesod
-- by overriding methods in the Yesod typeclass. That instance is
-- declared in the Foundation.hs file.
module Settings
    ( widgetFile
    , PersistConfig
    , staticRoot
    , staticDir
    , Extra (..)
    , parseExtra
    ) where

import Prelude
import Text.Shakespeare.Text (st)
import Language.Haskell.TH.Syntax
import Database.Persist.MySQL (MySQLConf)
import Yesod.Default.Config
--import qualified Yesod.Default.Util
import Data.Text (Text)
import Data.Yaml
import Control.Applicative
import Settings.Development
import Text.Coffee
import Text.Lucius (luciusFile, luciusFileReload)
import Text.Julius (juliusFile, juliusFileReload)
import Text.Cassius (cassiusFile, cassiusFileReload)
import System.Directory (doesFileExist) --, createDirectoryIfMissing)
import Data.Maybe (catMaybes)
import Control.Monad (when)
import Yesod.Core

-- | Which Persistent backend this site is using.
type PersistConfig = MySQLConf

-- Static setting below. Changing these requires a recompile

-- | The location of static files on your system. This is a file system
-- path. The default value works properly with your scaffolded site.
staticDir :: FilePath
staticDir = "static"

-- | The base URL for your static files. As you can see by the default
-- value, this can simply be "static" appended to your application root.
-- A powerful optimization can be serving static files from a separate
-- domain name. This allows you to use a web server optimized for static
-- files, more easily set expires and cache values, and avoid possibly
-- costly transference of cookies on static files. For more information,
-- please see:
--   http://code.google.com/speed/page-speed/docs/request.html#ServeFromCookielessDomain
--
-- If you change the resource pattern for StaticR in Foundation.hs, you will
-- have to make a corresponding change here.
--
-- To see how this value is used, see urlRenderOverride in Foundation.hs
staticRoot :: AppConfig DefaultEnv x -> Text
staticRoot conf = [st|#{appRoot conf}/static|]


-- The rest of this file contains settings which rarely need changing by a
-- user.

widgetFile :: String -> Q Exp
widgetFile = if development then widgetFileReload --Yesod.Default.Util.widgetFileReload
                            else widgetFileNoReload --Yesod.Default.Util.widgetFileNoReload


{-                            
widgetFileNoReload :: FilePath -> Q Exp
widgetFileNoReload x = do
    let h = whenExists x "hamlet"  whamletFile
    let c = whenExists x "coffee"  coffeeFile
    let l = whenExists x "lucius"  luciusFile
    [|$h >> addJulius $c >> addLucius $l|]

widgetFileReload :: FilePath -> Q Exp
widgetFileReload x = do
    let h = whenExists x "hamlet"  whamletFile
    let c = whenExists x "coffee"  coffeeFileReload
    let l = whenExists x "lucius"  luciusFileReload
    [|$h >> addJulius $c >> addLucius $l|]
-}                            


globFile :: String -> String -> FilePath
globFile kind x = "templates/" ++ x ++ "." ++ kind


widgetFileNoReload :: FilePath -> Q Exp
widgetFileNoReload x = combine "widgetFileNoReload" x
    [ whenExists x False "hamlet"  whamletFile
    , whenExists x True  "cassius" cassiusFile
    , whenExists x True  "julius"  juliusFile
    , whenExists x True  "lucius"  luciusFile
    , whenExists x True  "coffee"  coffeeFile
    ]


widgetFileReload :: FilePath -> Q Exp
widgetFileReload x = combine "widgetFileReload" x
    [ whenExists x False "hamlet"  whamletFile
    , whenExists x True  "cassius" cassiusFileReload
    , whenExists x True  "julius"  juliusFileReload
    , whenExists x True  "lucius"  luciusFileReload
    , whenExists x True  "coffee"  coffeeFileReload
    ]


combine :: String -> String -> [Q (Maybe Exp)] -> Q Exp
combine func file qmexps = do
    mexps <- sequence qmexps
    case catMaybes mexps of
        [] -> error $ concat
            [ "Called "
            , func
            , " on "
            , show file
            , ", but no template were found."
            ]
        exps -> return $ DoE $ map NoBindS exps


whenExists :: String
           -> Bool -- ^ requires toWidget wrap
           -> String -> (FilePath -> Q Exp) -> Q (Maybe Exp)
whenExists = warnUnlessExists False


warnUnlessExists :: Bool
                 -> String
                 -> Bool -- ^ requires toWidget wrap
                 -> String -> (FilePath -> Q Exp) -> Q (Maybe Exp)
warnUnlessExists shouldWarn x wrap glob f = do
    let fn = globFile glob x
    e <- qRunIO $ doesFileExist fn
    when (shouldWarn && not e) $ qRunIO $ putStrLn $ "widget file not found: " ++ fn
    if e
        then do
            ex <- f fn
            if wrap
                then do
                    tw <- [|toWidget|]
                    return $ Just $ tw `AppE` ex
                else return $ Just ex
        else return Nothing


data Extra = Extra
    { extraCopyright :: Text
    , extraAnalytics :: Maybe Text -- ^ Google Analytics
    } deriving Show

parseExtra :: DefaultEnv -> Object -> Parser Extra
parseExtra _ o = Extra
    <$> o .:  "copyright"
    <*> o .:? "analytics"
