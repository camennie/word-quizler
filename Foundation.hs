
{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module Foundation
    ( App (..)
    , Route (..)
    , AppMessage (..)
    , resourcesApp
    , Handler
    , Widget
    , Form
    , maybeAuth
    , requireAuth
    , module Settings
    , module Model
    ) where

import Prelude
import Yesod
import Yesod.Static
import Yesod.Auth
import Yesod.Auth.BrowserId
import Yesod.Auth.GoogleEmail
import Yesod.Auth.OpenId
import Yesod.Auth.Facebook.ClientSide
--import OpenId
import Yesod.Default.Config
import Yesod.Default.Util (addStaticContentExternal)
import Yesod.Logger (Logger, logMsg, formatLogText)
import Network.HTTP.Conduit (Manager)
import qualified Settings
import qualified Database.Persist.Store
import Settings.StaticFiles
import Database.Persist.GenericSql
import Settings (widgetFile, Extra (..))
import Model
import Text.Jasmine (minifym)
import Web.ClientSession (getKey)
import Text.Hamlet (hamletFile)
import Debug.Trace
import qualified Facebook as FB
--import Text.Blaze.Html (toHtml)
--import Data.Text



-- | The site argument for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { settings :: AppConfig DefaultEnv Extra
    , getLogger :: Logger
    , getStatic :: Static -- ^ Settings for static file serving.
    , connPool :: Database.Persist.Store.PersistConfigPool Settings.PersistConfig -- ^ Database connection pool.
    , httpManager :: Manager
    , persistConfig :: Settings.PersistConfig
    , fbCreds     :: FB.Credentials
    }

-- Set up i18n messages. See the message folder.
mkMessage "App" "messages" "en"

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/handler
--
-- This function does three things:
--
-- * Creates the route datatype AppRoute. Every valid URL in your
--   application can be represented as a value of this type.
-- * Creates the associated type:
--       type instance Route App = AppRoute
-- * Creates the value resourcesApp which contains information on the
--   resources declared below. This is used in Handler.hs by the call to
--   mkYesodDispatch
--
-- What this function does *not* do is create a YesodSite instance for
-- App. Creating that instance requires all of the handler functions
-- for our application to be in scope. However, the handler functions
-- usually require access to the AppRoute datatype. Therefore, we
-- split these actions into two functions and place them in separate files.
mkYesodData "App" $(parseRoutesFile "config/routes")

type Form x = Html -> MForm App App (FormResult x, Widget)


instance YesodAuthFbClientSide App where
    fbCredentials = fbCreds
    getFbChannelFile = return FbChannelFileR


-- $(staticFiles "static")

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    approot = ApprootMaster $ appRoot . settings

    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
    makeSessionBackend _ = do
        key <- getKey "config/client_session_key.aes"
        return . Just $ clientSessionBackend key 120

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        pc <- widgetToPageContent $ do
            $(widgetFile "normalize")
            -- addStylesheet $ StaticR css_bootstrap_css
            $(widgetFile "default-layout")
        hamletToRepHtml $(hamletFile "templates/default-layout-wrapper.hamlet")

    -- This is done to provide an optimization for serving static files from
    -- a separate domain. Please see the staticRoot setting in Settings.hs
    urlRenderOverride y (StaticR s) =
        Just $ uncurry (joinPath y (Settings.staticRoot $ settings y)) $ renderRoute s
    urlRenderOverride _ _ = Nothing

    -- The page to be redirected to when authentication is required.
    authRoute _ = Just $ AuthR LoginR

    messageLogger y loc level msg =
      formatLogText (getLogger y) loc level msg >>= logMsg (getLogger y)

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent = addStaticContentExternal minifym base64md5 Settings.staticDir (StaticR . flip StaticRoute [])

    -- Place Javascript at bottom of the body tag so the rest of the page loads first
    jsLoader _ = BottomOfBody

    errorHandler NotFound = custom404Handler
    errorHandler other = defaultErrorHandler other



-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlPersist
    runDB f = do
        master <- getYesod
        Database.Persist.Store.runPool
            (persistConfig master)
            f
            (connPool master)

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = FrontDoorR
    -- Where to send a user after logout
    logoutDest _ = HomeR

    getAuthId creds =
        runDB $ do
            x <- getBy $ UniqueUser $ credsIdent creds
            case x of
                Just (Entity uid _) -> return $ Just uid
                Nothing -> do
                    let emailM = case (lookup "openid.ax.value.email" $ credsExtra creds) of
                                    Just email -> Just email
                                    Nothing -> (lookup "openid.ext1.value.email" $ credsExtra creds)

                    fmap Just $ insert $ User (credsIdent creds) Nothing emailM True

    -- You can add other plugins like BrowserID, email or OAuth here
    --authPlugins _ = [authOpenId, authBrowserId, authGoogleEmail, authFacebookClientSide]
    authPlugins _ = [
                        (authOpenIdExtended [("openid.ns.ax", "http://openid.net/srv/ax/1.0"),
                                          ("openid.ax.mode", "fetch_request"),
                                          ("openid.ax.type.email", "http://axschema.org/contact/email"),
                                          ("openid.ax.required", "email")
                                         ])
                        { apLogin = \toMaster -> do
                                $(widgetFile "Header_NoLogin")
                                $(widgetFile "OpenIdAuth")
                                $(widgetFile "Footer")
                        }
                        ,
    
                        authFacebookClientSide { apLogin = \toMaster -> do
                                                toWidget [julius|
                                                        window.fbLoginButtonClicked = false;
                                                        window.fbLoginAlreadyAuth = false;

                                                        // Load the SDK Asynchronously
                                                        (function(d){
                                                           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                                                           if (d.getElementById(id)) {return;}
                                                           js = d.createElement('script'); js.id = id; js.async = true;
                                                           js.src = "//connect.facebook.net/en_US/all.js";
                                                           ref.parentNode.insertBefore(js, ref);
                                                         }(document));

                                                        // Init the SDK upon load
                                                        window.fbAsyncInit = function() {
                                                          FB.init({"cookie":true,"status":true,"appId":"000000000000000","channelUrl":"@{HomeR}fbchannelfile"});
                                                          

                                                          // Subscribe to statusChange event.
                                                          FB.Event.subscribe("auth.statusChange", function (response) {
                                                            if (response) {
                                                              // If the user is logged in on our site or not.
                                                              var loggedIn = false;

                                                              if (response.status === 'connected') {
                                                                window.fbLoginAlreadyAuth = true;

                                                                // Facebook says the user is logged in.
                                                                if (window.fbLoginButtonClicked == true) {
                                                                    window.location.href = '@{HomeR}/auth/page/fbcs/login';
                                                                }//if
                                                              } else {
                                                                // User is not logged in.
                                                              }
                                                            }
                                                          });
                                                        }

                                                        window.tryFBLogin = function() 
                                                        {
                                                            window.fbLoginButtonClicked = true;

                                                            if (window.fbLoginAlreadyAuth == true) {
                                                                window.location.href = '@{HomeR}/auth/page/fbcs/login';
                                                            } else {
                                                                FB.login(function () {});
                                                            }//if
                                                        }
                                                |]

                                                [whamlet|
                                                    <div #fb-root>
                                                    <a href="#" onclick="window.tryFBLogin()">Login with Facebook</a>
                                                |]
                                            }
                    ]

{-
    authPlugins app = [(authOpenIdExtended [("openid.ns.ax", "http://openid.net/srv/ax/1.0"),
                                          ("openid.ax.mode", "fetch_request"),
                                          ("openid.ax.type.email", "http://axschema.org/contact/email"),
                                          ("openid.ax.required", "email")
                                         ])
                        { apLogin = \toMaster -> do
                                $(widgetFile "Header_NoLogin")
                                $(widgetFile "OpenIdAuth")
                                $(widgetFile "Footer")
                        }
                      ]
-}
    authHttpManager = httpManager

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

-- Note: previous versions of the scaffolding included a deliver function to
-- send emails. Unfortunately, there are too many different options for us to
-- give a reasonable default. Instead, the information is available on the
-- wiki:
--
-- https://github.com/yesodweb/yesod/wiki/Sending-email


custom404Handler :: GHandler sub App ChooseRep
custom404Handler = 
    fmap chooseRep $ defaultLayout $ do
                    let maid = Nothing
                    setTitle "404 Page Not Found"
                    
                    $(widgetFile "Header_NoLogin")

                    toWidget [hamlet|
            <h1>Not Found
            <p>We apologize for the inconvenience, but the requested page could not be located.
            |]

                    $(widgetFile "Footer")


