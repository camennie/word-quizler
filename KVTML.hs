{-# LANGUAGE OverloadedStrings #-}


{-
    WordQuizler -- A Haskell/Yesod word/phrase quiz tool using the Leitner system
    Copyright (C) 2011-2013 Chris A. Mennie

    This file is part of WordQuizler.

    WordQuizler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordQuizler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WordQuizler.  If not, see <http://www.gnu.org/licenses/>.

-}



module KVTML where

import Data.Time
--import Data.Time.Clock (UTCTime)
import Data.Time.Format
import System.Locale (defaultTimeLocale)
import Import
import Data.Int
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.UTF8 as L8
import Text.XML.Light
import Data.Maybe (fromJust, isJust, catMaybes)
import Data.Text (append, pack, unpack, concat)
import Data.Either
import Data.List (head)
import Safe
import qualified Data.Map as DM
--import Codec.Binary.UTF8.String
import Debug.Trace



data KVTML = KVTML  {  
                        kvtml_quizVersion :: Int,
                        kvtml_title :: Text,
                        kvtml_language1 :: Text,
                        kvtml_language2 :: Text,
                        kvtml_leitnerLevels :: [Int],
                        kvtml_preferences :: KVTMLPreferences,
                        kvtml_quizLessons :: [KVTMLLesson], -- root lesson is lesson with smallest id
                        kvtml_quizEntries :: [KVTMLEntry]
                    }


defaultKVTML :: KVTML
defaultKVTML = KVTML  {  
                        kvtml_quizVersion = 1,
                        kvtml_title = "Quiz",
                        kvtml_language1 = "Language 1",
                        kvtml_language2 = "Language 2",
                        kvtml_leitnerLevels = [-1, 2, 3, 5, 8, 15, 30, 45, 60, 75, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130],
                        kvtml_preferences = defaultKVTMLPreferences,
                        kvtml_quizLessons = [defaultKVTMLLesson],
                        kvtml_quizEntries = []
                      }


data KVTMLPreferences = KVTMLPreferences {
                                            kvtml_doRealLeitner :: Bool,
                                            kvtml_maxEntriesPerSession :: Int,
                                            kvtml_minQuestionLocality :: Int,
                                            kvtml_maxQuestionLocality :: Int,
                                            kvtml_showWordCycle :: Bool,
                                            kvtml_numCorrectUntilAccepted :: Int,
                                            kvtml_resetCorrectCountOnIncorrect :: Bool,
                                            kvtml_flashcardModeRandom :: Bool,
                                            kvtml_fontSize :: Double
                                         }


defaultKVTMLPreferences :: KVTMLPreferences
defaultKVTMLPreferences = KVTMLPreferences {
                                                kvtml_doRealLeitner = True,
                                                kvtml_maxEntriesPerSession = -1,
                                                kvtml_minQuestionLocality = -1,
                                                kvtml_maxQuestionLocality = -1,
                                                kvtml_showWordCycle = True,
                                                kvtml_numCorrectUntilAccepted = 3,
                                                kvtml_resetCorrectCountOnIncorrect = True,
                                                kvtml_flashcardModeRandom = True,
                                                kvtml_fontSize = 6.5
                                           } 


data KVTMLLesson = KVTMLLesson  {
                                    kvtml_lessonId :: Int64, --id of lesson within quiz, not database
                                    kvtml_name :: Text,
                                    kvtml_category :: Text,
                                    kvtml_selected :: Bool,
                                    kvtml_childLessons :: [Int64],
                                    kvtml_entries :: [Int64]
                                }


defaultKVTMLLesson :: KVTMLLesson
defaultKVTMLLesson = KVTMLLesson {
                                    kvtml_lessonId = 0,
                                    kvtml_name = "New Lesson",
                                    kvtml_category = "",
                                    kvtml_selected = True,
                                    kvtml_childLessons = [],
                                    kvtml_entries = []
                                 }


data KVTMLEntry = KVTMLEntry { 
                                kvtml_entryID :: Int64, --id of entry within quiz, not database
                                kvtml_translation1 :: KVTMLEntryTranslation,
                                kvtml_translation2 :: KVTMLEntryTranslation
                             }


defaultKVTMLEntry :: KVTMLEntry
defaultKVTMLEntry = KVTMLEntry {
                                kvtml_entryID = 0,
                                kvtml_translation1 = defaultKVTMLEntryTranslation,
                                kvtml_translation2 = defaultKVTMLEntryTranslation
                               }


data KVTMLEntryTranslation = KVTMLEntryTranslation {
                                                        kvtml_text :: Text,
                                                        kvtml_currentGrade :: Int,
                                                        kvtml_count :: Int,
                                                        kvtml_errorCount :: Int,
                                                        kvtml_date :: UTCTime
                                                    }


defaultKVTMLEntryTranslation :: KVTMLEntryTranslation
defaultKVTMLEntryTranslation = KVTMLEntryTranslation {
                                                        kvtml_text = "XXXX",
                                                        kvtml_currentGrade = 0,
                                                        kvtml_count = 0,
                                                        kvtml_errorCount = 0,
                                                        kvtml_date = read "2012-01-01 01:00:00"
                                                    }


------------------------------------------------------------------------------------------------------------------


saveNewKVTML :: KVTML -> 
                        Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> 
                        Handler (Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App)))
saveNewKVTML kvtml ownerID = do
        quizID <- runDB $ insert quizBase
        let entries = convertKVTMLEntries kvtml quizID
        let lessons = convertKVTMLLessons kvtml quizID

        runDB $ (insertEntries entries >> insertLessons lessons)
        return quizID
    where
        quizBase = convertKVTML kvtml ownerID

        insertEntries [] = return ()
        insertEntries (x:xs) = insert x >> insertEntries xs

        insertLessons [] = return ()
        insertLessons (x:xs) = insert x >> insertLessons xs
    

convertKVTML :: KVTML -> 
                Key (YesodPersistBackend App) (UserGeneric (YesodPersistBackend App)) -> 
                ZQuizBase
convertKVTML kvtml ownerID =
        ZQuizBase {
            zQuizBaseVersion = kvtml_quizVersion kvtml,
            zQuizBaseOwningUserId = ownerID,
            zQuizBaseLanguage1 = kvtml_language1 kvtml,
            zQuizBaseLanguage2 = kvtml_language2 kvtml,
            zQuizBaseTitle = kvtml_title kvtml,
            zQuizBaseLeitnerLevels = kvtml_leitnerLevels kvtml,
            zQuizBaseRootLesson = 0,
            zQuizBaseRealLeitner = kvtml_doRealLeitner preferences,
            zQuizBaseMinQuestionLocality = kvtml_minQuestionLocality preferences,
            zQuizBaseMaxQuestionLocality = kvtml_maxQuestionLocality preferences,
            zQuizBaseShowWordCycle = kvtml_showWordCycle preferences,
            zQuizBaseNumRepeatUntilAccepted = kvtml_numCorrectUntilAccepted preferences,
            zQuizBaseResetCountOnWrong = kvtml_resetCorrectCountOnIncorrect preferences,
            zQuizBaseRandomFlashcardMode = kvtml_flashcardModeRandom preferences,
            zQuizBaseMaxSessionEntries = kvtml_maxEntriesPerSession preferences,
            zQuizBaseFontSize = kvtml_fontSize preferences
        }

    where
        preferences = kvtml_preferences kvtml :: KVTMLPreferences


convertKVTMLLessons ::  KVTML -> 
                        Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App)) ->
                        [ZQuizLesson]
convertKVTMLLessons kvtml quizID =
        map toLesson $ kvtml_quizLessons kvtml
    where
        toLesson lesson =
                ZQuizLesson {
                    zQuizLessonOwningQuizId = quizID,
                    zQuizLessonQuizLessonID = kvtml_lessonId lesson,
                    zQuizLessonName = kvtml_name lesson,
                    zQuizLessonCategory = kvtml_category lesson,
                    zQuizLessonSelected = kvtml_selected lesson,
                    zQuizLessonChildLessons = kvtml_childLessons lesson,
                    zQuizLessonEntries = kvtml_entries lesson
                }


convertKVTMLEntries ::  KVTML -> 
                        Key (YesodPersistBackend App) (ZQuizBaseGeneric (YesodPersistBackend App)) ->
                        [ZEntryItems]
convertKVTMLEntries kvtml quizID =
        map toEntryItem $ kvtml_quizEntries kvtml
    where
        toEntryItem entry =
                ZEntryItems {
                    zEntryItemsOwningQuizId = quizID,
                    zEntryItemsQuizEntryID = kvtml_entryID entry,
                    zEntryItemsCount1 = kvtml_count translation1,
                    zEntryItemsCount2 = kvtml_count translation2,
                    zEntryItemsCurrentGrade1 = kvtml_currentGrade translation1,
                    zEntryItemsCurrentGrade2 = kvtml_currentGrade translation2,
                    zEntryItemsErrorCount1 = kvtml_errorCount translation1,
                    zEntryItemsErrorCount2 = kvtml_errorCount translation2,
                    zEntryItemsDate1 = kvtml_date translation1,
                    zEntryItemsDate2 = kvtml_date translation2,
                    zEntryItemsText1 = kvtml_text translation1,
                    zEntryItemsText2 = kvtml_text translation2
                }
            where
                translation1 = kvtml_translation1 entry
                translation2 = kvtml_translation2 entry


------------------------------------------------------------------------------------------------------------------

{-
loadKVTML :: ZQuizBase -> [ZQuizLesson] -> [ZEntryItems] -> KVTML
loadKVTML dbQuiz dbLessons dbEntries = KVTML  {  
                        kvtml_quizVersion = zQuizBaseVersion dbQuiz,
                        kvtml_title = zQuizBaseTitle dbQuiz,
                        kvtml_language1 = zQuizBaseLanguage1 dbQuiz,
                        kvtml_language2 = zQuizBaseLanguage2 dbQuiz,
                        kvtml_leitnerLevels = zQuizBaseLeitnerLevels dbQuiz,
                        kvtml_preferences = dbPreferences,
                        kvtml_quizLessons = quizLessons dbLessons,
                        kvtml_quizEntries = quizEntries dbEntries
                      }
        where
            dbPreferences = KVTMLPreferences {
                                                kvtml_doRealLeitner = zQuizBaseRealLeitner dbQuiz,
                                                kvtml_maxEntriesPerSession = zQuizBaseMaxSessionEntries dbQuiz,
                                                kvtml_minQuestionLocality = zQuizBaseMinQuestionLocality dbQuiz,
                                                kvtml_maxQuestionLocality = zQuizBaseMaxQuestionLocality dbQuiz,
                                                kvtml_showWordCycle = zQuizBaseShowWordCycle dbQuiz,
                                                kvtml_numCorrectUntilAccepted = zQuizBaseNumRepeatUntilAccepted dbQuiz,
                                                kvtml_resetCorrectCountOnIncorrect = zQuizBaseResetCountOnWrong dbQuiz,
                                                kvtml_flashcardModeRandom = zQuizBaseRandomFlashcardMode dbQuiz,
                                                kvtml_fontSize = zQuizBaseFontSize dbQuiz
                                           }

            quizLessons lessons = (flip map) lessons $
                                    \l -> KVTMLLesson   {
                                                            kvtml_lessonId = zQuizLessonQuizLessonID l,
                                                            kvtml_name = zQuizLessonName l,
                                                            kvtml_category = zQuizLessonCategory l,
                                                            kvtml_selected = zQuizLessonSelected l,
                                                            kvtml_childLessons = zQuizLessonChildLessons l,
                                                            kvtml_entries = zQuizLessonEntries l
                                                        }

            quizEntries entries = (flip map) entries $
                                    \e -> KVTMLEntry {
                                                        kvtml_entryID = zEntryItemsQuizEntryID e,
                                                        kvtml_translation1 =
                                                            KVTMLEntryTranslation {
                                                                                    kvtml_text = zEntryItemsText1 e,
                                                                                    kvtml_currentGrade = zEntryItemsCurrentGrade1 e,
                                                                                    kvtml_count = zEntryItemsCount1 e,
                                                                                    kvtml_errorCount = zEntryItemsErrorCount1 e,
                                                                                    kvtml_date = zEntryItemsDate1 e
                                                                                  },
                                                        kvtml_translation2 =
                                                            KVTMLEntryTranslation {
                                                                                    kvtml_text = zEntryItemsText2 e,
                                                                                    kvtml_currentGrade = zEntryItemsCurrentGrade2 e,
                                                                                    kvtml_count = zEntryItemsCount2 e,
                                                                                    kvtml_errorCount = zEntryItemsErrorCount2 e,
                                                                                    kvtml_date = zEntryItemsDate2 e
                                                                                  }
                                                     }
-}


------------------------------------------------------------------------------------------------------------------

data ContainerInfo = ContainerInfo { ci_name :: Text, ci_entries :: [Int64], ci_numChildren :: Int } deriving (Show)
type CIMap = DM.Map [Int] ContainerInfo
type CIIDMap = DM.Map [Int] (ContainerInfo, Int64)


importKVTML :: L.ByteString -> Either Text KVTML
importKVTML fileData =
    let parsedXMLDoc = parseXMLDoc $ L8.toString fileData in

    case parsedXMLDoc of
        Nothing -> Left "Invalid XML file. Couldn't parse."
        Just rootElement -> do
                title <- getKVTMLTitle rootElement
                kvtmlLanguage1 <- getKVTMLLanguage rootElement 0
                kvtmlLanguage2 <- getKVTMLLanguage rootElement 1
                quizEntries <- getKVTMLEntries rootElement
                quizLessons <- getKVTMLLessons rootElement

                let baseKVTML = KVTML  {  
                                    kvtml_quizVersion = 1,
                                    kvtml_title = title,
                                    kvtml_language1 = kvtmlLanguage1,
                                    kvtml_language2 = kvtmlLanguage2,
                                    kvtml_leitnerLevels = [-1, 2, 3, 5, 8, 15, 30, 45, 60, 75, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130],
                                    kvtml_preferences = defaultKVTMLPreferences,
                                    kvtml_quizLessons = quizLessons,
                                    kvtml_quizEntries = quizEntries
                                  }

                let kvtml = baseKVTML

                return kvtml


getKVTMLLanguage :: Element -> Int -> Either Text Text
getKVTMLLanguage rootElement index = 
        nameNode
    where
        nameNode = case nameNodeList of
                            Left msg -> Left msg
                            Right [] -> Left $ "Couldn't find name node for index " `append` (pack $ show index)
                            Right (nameNodeEl:_) -> Right $ pack $ strContent nameNodeEl

        nameNodeList = getNameNodeList identifierNodeList
        getNameNodeList (Left msg) = Left msg
        getNameNodeList (Right []) = Left $ "Couldn't find identifier node for index " `append` (pack $ show index)
        getNameNodeList (Right (actualIdentifierNode:_)) = Right $ (flip filterChildrenName) actualIdentifierNode (\qn -> (==) "name" $ qName qn)

        identifierNodeList = getIdentifierNodeList identifiersNodeList
        identifiersNodeList = (flip filterChildrenName) rootElement (\qn -> (==) "identifiers" $ qName qn)
        getIdentifierNodeList [] = Left "Couldn't find identifiers node."
        getIdentifierNodeList (actualIdentifiersNode:_) = Right $ (flip filterChildren) actualIdentifiersNode filterIdentifiersNodeChildren

        filterIdentifiersNodeChildren :: Element -> Bool
        filterIdentifiersNodeChildren enode =
            if ((==) "identifier" $ qName $ elName enode) &&
               (isJust $ findAttr (QName "id" Nothing Nothing) enode)
                then 
                    if (==) (fromJust $ findAttr (QName "id" Nothing Nothing) enode) $ show index
                        then True
                        else False
                else False


getKVTMLTitle :: Element -> Either Text Text
getKVTMLTitle rootElement =
        titleElement
    where
        titleElement = case titleElementList of
                            Left msg -> Left msg
                            Right [] -> Left "Couldn't find title node." 
                            Right (titleNode:_) -> Right $ pack $ strContent titleNode

        titleElementList = getTitleElementList informationElementList
        informationElementList = (flip filterChildrenName) rootElement (\qn -> (==) "information" $ qName qn)
        getTitleElementList [] = Left "Couldn't find information node."
        getTitleElementList (infoNode:_) = Right $ (flip filterChildrenName) infoNode (\qn -> (==) "title" $ qName qn)


getKVTMLLessons :: Element -> Either Text [KVTMLLesson]
getKVTMLLessons rootElement = do
        lessonsNode <- getChildNode rootElement "lessons"
        let ciidMap = createCIIDMap lessonsNode
        let flattenedCIIMap = DM.toList ciidMap
        let lessons = (flip map) flattenedCIIMap $ convertCIIDToKVTMLLesson ciidMap

        return $ ensureSingleTopmostLesson lessons lessonsNode ciidMap


ensureSingleTopmostLesson :: [KVTMLLesson] -> Element -> CIIDMap -> [KVTMLLesson]
ensureSingleTopmostLesson [] _ _ = [defaultKVTMLLesson]
ensureSingleTopmostLesson [singleLesson] _ _ = [singleLesson { kvtml_lessonId = 0 }]
ensureSingleTopmostLesson lessons lessonsNode ciidMap =
        [rootLesson] ++ lessons
    where
        rootLesson =    KVTMLLesson  {
                                        kvtml_lessonId = 0,
                                        kvtml_name = "Root Lesson",
                                        kvtml_category = "",
                                        kvtml_selected = True,
                                        kvtml_childLessons = catMaybes childLessonNums,
                                        kvtml_entries = []
                                     }

        childLessons = getChildNodes lessonsNode "container"
        childLessonNums = (flip map) [1..length childLessons]
                            (\idx -> case (DM.lookup [idx] ciidMap) of
                                        Nothing -> (trace ("Couldn't find lesson in map1!" ++ (show [idx]))) Nothing
                                        Just (_, inner_ciid) -> Just inner_ciid
                            )


convertCIIDToKVTMLLesson :: CIIDMap -> ([Int], (ContainerInfo, Int64)) -> KVTMLLesson
convertCIIDToKVTMLLesson ciidMap (zipperPath, (ci, ciid)) =
        convertedKVTMLLesson
    where
        convertedKVTMLLesson = KVTMLLesson  {
                                                kvtml_lessonId = ciid,
                                                kvtml_name = ci_name ci,
                                                kvtml_category = "",
                                                kvtml_selected = True,
                                                kvtml_childLessons = catMaybes childLessonsM, -- XXX: We handle errors, but don't pass them on
                                                kvtml_entries = ci_entries ci
                                            }

        childLessonsM = (flip map) [1..ci_numChildren ci]
                            (\idx -> case (DM.lookup (zipperPath ++ [idx]) ciidMap) of
                                        Nothing -> (trace ("Couldn't find lesson in map2! " ++ (show (zipperPath ++ [idx])))) Nothing
                                        Just (_, inner_ciid) -> Just inner_ciid
                            )


createCIIDMap :: Element -> CIIDMap
createCIIDMap lessonsNode =
        ciidMap
    where
        ciMap = createCIMap lessonsNode [] DM.empty
        flattenedCIMap = DM.toList ciMap

        zippedFlattenedCIList :: [(([Int], ContainerInfo), Int64)]
        zippedFlattenedCIList = zip flattenedCIMap [1..]

        zipperCIIdList :: [([Int], (ContainerInfo, Int64))]
        zipperCIIdList = (flip map) zippedFlattenedCIList (\((zipper, ci), ciid) -> (zipper, (ci, ciid)))

        ciidMap :: CIIDMap
        ciidMap = DM.fromList zipperCIIdList


createCIMap :: Element -> [Int] -> CIMap -> CIMap
createCIMap lesson lessonZipper ciMap =
        finalCIMap
    where
        childContainers :: Element -> [Element]
        childContainers cnode = (flip filterChildrenName) cnode (\qn -> (==) "container" $ qName qn)

        childContainersZ :: [(Element, Int)]
        childContainersZ = zip (childContainers lesson) [1..]

        childrenCIList :: [([Int], ContainerInfo)]
        childrenCIList = (flip map) childContainersZ (\(enode, idx) -> (lessonZipper ++ [idx], getContainerInfo enode $ length $ childContainers enode))

        foldedChildrenMap :: CIMap
        foldedChildrenMap = foldr (\(zipper, ci) accMap -> DM.insert zipper ci accMap) ciMap childrenCIList

        childCIMaps :: [CIMap]
        childCIMaps = (flip map) childContainersZ (\(enode, idx) -> createCIMap enode (lessonZipper ++ [idx]) DM.empty)

        finalCIMap = DM.unions $ childCIMaps ++ [foldedChildrenMap]


getContainerInfo :: Element -> Int -> ContainerInfo
getContainerInfo containerNode numChildren =
        ContainerInfo { ci_name = name, ci_entries = entryInts, ci_numChildren = numChildren }
    where
        name = case getNodeStr containerNode "name" of
                    Left _ -> "Lesson" -- XXX: We're ignoring name attribute errors here
                    Right "" -> "Lesson" -- XXX: We're ignoring name attribute errors here
                    Right nm -> nm

        entriesList = (flip filterChildrenName) containerNode (\qn -> (==) "entry" $ qName qn)
        entryIdEList = (flip map) entriesList $ getNodeAttrStrInt64 "id" 
        entryInts = rights entryIdEList -- XXX: We're ignoring id errors here


getKVTMLEntries :: Element -> Either Text [KVTMLEntry]
getKVTMLEntries rootElement =
        if null entryLefts
            then Right $ rights eitherEntriesList
            else Left $ head entryLefts
    where
        entryLefts = lefts eitherEntriesList

        eitherEntriesList = case entryElementList of
                                Left msg -> [Left msg]
                                Right entries -> map parseEitherEntry entries

        entryElementList = getEntryElementList entriesElementList
        entriesElementList = (flip filterChildrenName) rootElement (\qn -> (==) "entries" $ qName qn)
        getEntryElementList [] = Left "Couldn't find entries node."
        getEntryElementList (entryNode:_) = Right $ (flip filterChildrenName) entryNode (\qn -> (==) "entry" $ qName qn)


parseEitherEntry :: Element -> Either Text KVTMLEntry
parseEitherEntry entryNode = do
    entryID <- getNodeAttrStrInt64 "id" entryNode
    translationNode0 <- getTranslationNode entryNode 0
    translationNode1 <- getTranslationNode entryNode 1
    gradeNode0 <- getChildNode translationNode0 "grade"
    gradeNode1 <- getChildNode translationNode1 "grade"

    text0 <- getNodeStr translationNode0 "text"
    text1 <- getNodeStr translationNode1 "text"
    currentGrade0 <- getNodeStrInt gradeNode0 "currentgrade"
    currentGrade1 <- getNodeStrInt gradeNode1 "currentgrade"
    count0 <- getNodeStrInt gradeNode0 "count"
    count1 <- getNodeStrInt gradeNode1 "count"
    errorCount0 <- getNodeStrInt gradeNode0 "errorcount"
    errorCount1 <- getNodeStrInt gradeNode1 "errorcount"
    date0 <- getNodeStrDate gradeNode0 "date"
    date1 <- getNodeStrDate gradeNode1 "date"

    return KVTMLEntry {
                        kvtml_entryID = entryID,
                        kvtml_translation1 = 
                            KVTMLEntryTranslation {
                                                    kvtml_text = text0,
                                                    kvtml_currentGrade = currentGrade0,
                                                    kvtml_count = count0,
                                                    kvtml_errorCount = errorCount0,
                                                    kvtml_date = date0
                                                  },

                        kvtml_translation2 = 
                            KVTMLEntryTranslation {
                                                    kvtml_text = text1,
                                                    kvtml_currentGrade = currentGrade1,
                                                    kvtml_count = count1,
                                                    kvtml_errorCount = errorCount1,
                                                    kvtml_date = date1
                                                  }
                      }


getNodeAttrStrInt64 :: String -> Element -> Either Text Int64
getNodeAttrStrInt64 attrName enode = do
        attrValStr <- attrValE
        let attrValIntM = readMay attrValStr
        case attrValIntM of
                Nothing -> Left $ "Unable to parse id attribute to int on node " `append` (pack . qName $ elName enode)
                Just attrVal64 -> Right attrVal64
    where
        attrValE = case attrValM of
                        Nothing -> Left $ "Unable to find id attribute on node " `append` (pack . qName $ elName enode)
                        Just attrStr -> Right attrStr
        attrValM = findAttr (QName attrName Nothing Nothing) enode



getTranslationNode :: Element -> Int -> Either Text Element
getTranslationNode enode index =
        getTranslationNodeHelper translationNodeList
    where
        getTranslationNodeHelper [] = Left $ Data.Text.concat ["Unable to find translation node with index ", (pack $ show index), " for entry ", (pack $ show enode)]
        getTranslationNodeHelper (translationNode:_) = Right translationNode

        translationNodeList = (flip filterChildren) enode filterNodeChildren

        filterNodeChildren :: Element -> Bool
        filterNodeChildren en =
            if ((==) "translation" $ qName $ elName en) &&
               (isJust $ findAttr (QName "id" Nothing Nothing) en)
                then 
                    if (==) (fromJust $ findAttr (QName "id" Nothing Nothing) en) $ show index
                        then True
                        else False
                else False


getChildNodes :: Element -> String -> [Element]
getChildNodes enode childName =
    (flip filterChildrenName) enode (\qn -> (==) childName $ qName qn)


getChildNode :: Element -> String -> Either Text Element
getChildNode enode childName =
    case (findChild (QName childName Nothing Nothing) enode) of 
            Nothing -> Left $ Data.Text.concat ["Unable to find child node ", pack childName, " under node ", pack . qName $ elName enode]
            Just en -> Right en


getNodeStr :: Element -> String -> Either Text Text
getNodeStr enode childName = do
        childNode <- getChildNode enode childName
        Right . pack $ strContent childNode


getNodeStrInt :: Element -> String -> Either Text Int
getNodeStrInt enode childName = do
        childNodeStr <- getNodeStr enode childName
        let childNodeValM = readMay $ unpack childNodeStr
        case childNodeValM of
                Nothing -> Left $ "Unable to parse node str as int on node " `append` (pack . qName $ elName enode)
                Just attrValInt -> Right attrValInt


getNodeStrDate :: Element -> String -> Either Text UTCTime
getNodeStrDate enode childName = do
        childNodeStr <- getNodeStr enode childName
        let childNodeValM = Data.Time.Format.parseTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S" $ unpack childNodeStr
        case childNodeValM of
                Nothing -> Right $ UTCTime { utctDay = ModifiedJulianDay 2456086, utctDayTime = 0 } -- 2456086 = June 7 2012
                Just attrValDate -> Right attrValDate
                --Nothing -> Left $ Data.Text.concat ["Unable to parse node str as date on node ", (pack . qName $ elName node), " string was: '", childNodeStr , "'"]






